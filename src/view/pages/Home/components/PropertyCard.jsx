import _ from "lodash";
import Slider from "react-slick";
import EmptyImage from "assets/media/EmptyImage.png";
import { useNavigate } from "react-router-dom";

const PropertyCard = ({ property }) => {
  const navigate = useNavigate();

  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <div className="veshm-list-wraps">
      {property.nature == 1 ? (
        <div className="veshm-type">
          <span>For Rent</span>
        </div>
      ) : (
        <div className="veshm-type fr-sale">
          <span>For Sale</span>
        </div>
      )}
      <div className="veshm-list-thumb">
        <div className="veshm-list-img-slide">
          <div className="veshm-list-click">
            <Slider {...settings}>
              {!_.isEmpty(property.galleries) ? (
                property.galleries.map((elt, k2) => {
                  return (
                    <a
                      key={k2}
                      onClick={() => navigate(`/properties/${property.id}`)}
                      className="cursor-pointer"
                    >
                      <div style={{ height: "200px" }}>
                        <img
                          src={elt.imageSrc}
                          className="img-fluid mx-auto"
                          alt=""
                        />
                      </div>
                    </a>
                  );
                })
              ) : (
                <img
                  onClick={() => navigate(`/properties/${property.id}`)}
                  className="cursor-pointer"
                  src={EmptyImage}
                />
              )}
            </Slider>
          </div>
        </div>
      </div>
      <div className="veshm-list-block">
        <div className="veshm-tags trending">
          <span>
            <i className="fa-solid fa-meteor" />
            Trending
          </span>
        </div>
        <div className="veshm-list-head">
          <div className="veshm-list-head-caption">
            <div className="rlhc-price">
              <h4 className="rlhc-price-name theme-cl">
                ${property.price}
                <span className="monthly">/Months</span>
              </h4>
            </div>
            <div className="listing-short-detail-flex">
              <h5 className="rlhc-title-name verified">
                <a href="single-property-1.html" className="prt-link-detail">
                  {property.name}
                </a>
              </h5>
              <div className="rlhc-prt-location">
                <img
                  src="/src/assets/media/pin.svg"
                  width={16}
                  className="me-1"
                  alt=""
                />
                {property.address}
              </div>
            </div>
          </div>
          <div className="veshm-list-head-flex">
            <button className="btn btn-like" type="button">
              <i className="fa-solid fa-heart-circle-check" />
            </button>
          </div>
        </div>
        <div className="veshm-list-footer">
          <div className="veshm-list-circls">
            <ul>
              <li>
                <span className="bed-inf">
                  <i className="fa-solid fa-bed" />
                </span>
                {property.bedrooms} Bed
              </li>
              <li>
                <span className="bath-inf">
                  <i className="fa-solid fa-bath" />
                </span>
                {property.bathrooms} Ba
              </li>
              <li>
                <span className="area-inf">
                  <i className="fa-solid fa-vector-square" />
                </span>
                {property.space} Sft
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PropertyCard;
