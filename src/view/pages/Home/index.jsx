import { useEffect, useState } from "react";
import api from "api";
import PropertyCard from "./components/PropertyCard";
import Empty from "components/Empty";
import _ from "lodash";
import Loading from "components/Loading";

const Index = (props) => {
  const [properties, setProperties] = useState([]);
  const [searchCity, setSearchCity] = useState("");
  const [searchNature, setSearchNature] = useState(0);
  const [isLoading, setIsLoading] = useState(true);

  const fetchProperties = (name = "", nature = 0) => {
    api
      .get(`estates/search?name=${name}&nature=${nature}`)
      .then((response) => {
        setProperties(response.data);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
      });
  };
  const setSearchText = (event) => {
    var searchText = event.target.value;
    setSearchCity(searchText);
  };
  const launchSearch = () => {
    fetchProperties(searchCity, searchNature);
  };

  useEffect(() => {
    fetchProperties();
  }, []);
  return (
    <>
      <div
        className="image-bg hero-header"
        style={{
          background: "#2540a2 url(/src/assets/media/banner-2.jpg) no-repeat",
        }}
        data-overlay={0}
      >
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-9 col-md-11 col-sm-12">
              <div className="inner-banner-text text-center">
                <h1>
                  Discover A Beautiful
                  <br />
                  Place With Us
                </h1>
                <p className="text-light">
                  Would you explore nature paradise in the world, let't find the
                  best property in California withus.
                </p>
              </div>
              <div className="search-from-clasic mt-5">
                <div className="hero-search-content">
                  <div className="row">
                    <div className="col-xl-10 col-lg-10 col-md-9 col-sm-12">
                      <div className="classic-search-box">
                        <div className="form-group">
                          <div className="choose-propert-type">
                            <ul className="nav nav-pills" role="tablist">
                              <li className="nav-item" role="presentation">
                                <button
                                  className={
                                    "nav-link " +
                                    (searchNature == 0 ? "active" : "")
                                  }
                                  id="for-rent"
                                  data-bs-toggle="tab"
                                  data-bs-target="#rents"
                                  type="button"
                                  role="tab"
                                  aria-selected="true"
                                  onClick={() => setSearchNature(0)}
                                >
                                  All
                                </button>
                              </li>
                              <li className="nav-item" role="presentation">
                                <button
                                  className={
                                    "nav-link " +
                                    (searchNature == 1 ? "active" : "")
                                  }
                                  id="for-rent"
                                  data-bs-toggle="tab"
                                  data-bs-target="#rents"
                                  type="button"
                                  role="tab"
                                  aria-selected="true"
                                  onClick={() => setSearchNature(1)}
                                >
                                  For Rent
                                </button>
                              </li>
                              <li className="nav-item" role="presentation">
                                <button
                                  className={
                                    "nav-link " +
                                    (searchNature == 2 ? "active" : "")
                                  }
                                  id="for-buy"
                                  data-bs-toggle="tab"
                                  data-bs-target="#buys"
                                  type="button"
                                  role="tab"
                                  aria-selected="false"
                                  onClick={() => setSearchNature(2)}
                                >
                                  For Buy
                                </button>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div className="form-group full">
                          <div className="input-with-icon">
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Select city"
                              onChange={(e) => setSearchText(e)}
                            />
                            <img
                              src="/src/assets/media//pin.svg"
                              width={20}
                              alt=""
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-2 col-lg-2 col-md-3 col-sm-12">
                      <div className="form-group">
                        <button
                          type="submit"
                          className="btn btn-primary full-width"
                          onClick={() => launchSearch()}
                        >
                          Search
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section className="mid">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-8 col-md-10 text-center">
              <div className="sec-heading center mb-4">
                <h2>Recent Property</h2>
                <p>
                  At vero eos et accusamus et iusto odio dignissimos ducimus qui
                  blanditiis praesentium voluptatum deleniti atque corrupti quos
                  dolores
                </p>
              </div>
            </div>
          </div>
          <div className="row justify-content-center gx-3 gy-4">
            {" "}
            <Loading isLoading={isLoading}>
              {!_.isEmpty(properties) ? (
                properties.map((item, key) => {
                  return (
                    <div
                      key={key}
                      className="col-xl-3 col-lg-4 col-md-6 col-sm-12"
                    >
                      <PropertyCard key={key} property={item} />
                    </div>
                  );
                })
              ) : (
                <div className="d-flex justify-content-center">
                  <Empty
                    description={"No properties."}
                    img_heigth="120px"
                    img_width="120px"
                  />
                </div>
              )}
            </Loading>
          </div>
        </div>
      </section>
    </>
  );
};

export default Index;
