import { useLocation, useNavigate } from "react-router-dom";
import HeaderAccount from "./components/HeaderAccount/index";
import { useState } from "react";
import AccountDropdown from "./components/HeaderAccount/components/AccountDropdown";
import ResponsiveMenu from "./components/ResponsiveMenu";
import { menuList, renderMenuIdWhenRefresh } from "./components/MenuList";
import { useMediaQuery } from "react-responsive";
import { useAuth } from "components/AuthContext";
import Login from "pages/Auth/Login";
import Register from "pages/Auth/Register";
import Logo from "assets/media/booking-logo.png";
const Index = () => {
  const navigate = useNavigate();
  const auth = useAuth();
  const location = useLocation();
  const pathname = location.pathname;
  const [menuId, setMenuId] = useState(renderMenuIdWhenRefresh(pathname));
  const isMobile = useMediaQuery({ query: "(max-width: 992px)" });

  return (
    <>
      <div className="header header-light head-shadow">
        <div className="container">
          <nav id="navigation" className="navigation navigation-landscape">
            <div
              className="nav-header"
              style={{ justifyContent: "space-between" }}
            >
              {isMobile && <ResponsiveMenu />}
              <a className="nav-brand" onClick={() => navigate("/")}>
                <img src={Logo} className="logo" alt="" />
              </a>
              <div className="mobile_nav">
                {auth.loggedIn ? (
                  <ul className="text-right dhsbrd">
                    <li>
                      <AccountDropdown />
                    </li>
                  </ul>
                ) : (
                  <ul>
                    <li className="list-buttons">
                      <a
                        className="cursor-pointer"
                        onClick={() => auth.showLoginModal(true)}
                      >
                        <i className="fas fa-sign-in-alt me-2" />
                        Sign In
                      </a>
                    </li>
                  </ul>
                )}
              </div>
            </div>
            {!isMobile && (
              <div
                className="nav-menus-wrapper"
                style={{ transitionProperty: "none" }}
              >
                <ul className="nav-menu">
                  {menuList.map((item, key) => (
                    <li key={key} className={item.id == menuId ? "active" : ""}>
                      <a
                        onClick={() => {
                          navigate(item.link);
                          setMenuId(item.id);
                        }}
                        className="cursor-pointer"
                      >
                        {item.name}
                        <span className="submenu-indicator" />
                      </a>
                    </li>
                  ))}
                </ul>
                <HeaderAccount />
              </div>
            )}
          </nav>
        </div>
      </div>
      {auth.loginModalShow && (
        <Login
          show={auth.loginModalShow}
          onHide={() => auth.showLoginModal(false)}
        />
      )}
      {auth.registerModalShow && (
        <Register
          show={auth.registerModalShow}
          onHide={() => auth.showRegisterModal(false)}
        />
      )}
      <div className="clearfix" />
    </>
  );
};

export default Index;
