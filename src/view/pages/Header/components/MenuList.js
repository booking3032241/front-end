export const menuList = [
  {
    id: 1,
    name: "Home",
    link: "/",
  },
  {
    id: 2,
    name: "Properties",
    link: "/properties",
  },
];
export const renderMenuIdWhenRefresh = (pathname) => {
  if (pathname.includes("properties")) {
    return 2;
  }

  return 1;
};
