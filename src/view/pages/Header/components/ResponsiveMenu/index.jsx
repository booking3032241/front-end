import { useState } from "react";
import { ListGroup, Offcanvas } from "react-bootstrap";
import { BsListUl } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import { menuList } from "../MenuList";

const Index = () => {
  const navigate = useNavigate();

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [menuId, setMenuId] = useState(1);
  function goTo(link) {
    navigate(link);
    handleClose();
  }
  return (
    <>
      <BsListUl className="cursor-pointer fs-1" onClick={handleShow} />

      <Offcanvas show={show} onHide={handleClose}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title></Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <ListGroup>
            {menuList.map((item, key) => (
              <ListGroup.Item
                className={item.id == menuId ? "active" : ""}
                key={key}
                action
                onClick={() => {
                  goTo(item.link);
                  setMenuId(item.id);
                }}
              >
                {item.name}
              </ListGroup.Item>
            ))}
          </ListGroup>
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
};

export default Index;
