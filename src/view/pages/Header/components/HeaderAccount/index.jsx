import { useAuth } from "components/AuthContext";
import AccountDropdown from "./components/AccountDropdown";

const Index = () => {
  const auth = useAuth();

  return (
    <>
      <ul className="nav-menu nav-menu-social align-to-right dhsbrd">
        {auth.loggedIn ? (
          <>
            <li>
              <div className="btn-group account-drop">
                <button
                  type="button"
                  className="btn btn-order-by-filt"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <i className="fa-regular fa-bell" />
                  <span className="noti-status" />
                </button>
                {/* <div className="dropdown-menu pull-right animated flipInX">
                  <div className="drp_menu_headr">
                    <h4>Notifications</h4>
                  </div>
                  <div className="ntf-list-groups">
                    <div className="ntf-list-groups-single">
                      <div className="ntf-list-groups-icon text-purple">
                        <i className="fa-solid fa-house-medical-circle-check" />
                      </div>
                      <div className="ntf-list-groups-caption">
                        <p className="small">
                          Hi, Nothan your <strong>Vesh</strong> property
                          uploaded successfully
                        </p>
                      </div>
                    </div>
                    <div className="ntf-list-groups-single">
                      <div className="ntf-list-groups-icon text-warning">
                        <i className="fa-solid fa-envelope" />
                      </div>
                      <div className="ntf-list-groups-caption">
                        <p className="small">
                          You have got 2 message from{" "}
                          <strong className="text-success">Daniel</strong> 2
                          days ago
                        </p>
                      </div>
                    </div>
                    <div className="ntf-list-groups-single">
                      <div className="ntf-list-groups-icon text-success">
                        <i className="fa-solid fa-sack-dollar" />
                      </div>
                      <div className="ntf-list-groups-caption">
                        <p className="small">
                          Hi Nothan, Your fund <strong>$70,540</strong> transfer
                          successfully in your account
                        </p>
                      </div>
                    </div>
                    <div className="ntf-list-groups-single">
                      <div className="ntf-list-groups-icon text-danger">
                        <i className="fa-solid fa-comments" />
                      </div>
                      <div className="ntf-list-groups-caption">
                        <p className="small">
                          2 New agent send you report messages 5 days ago
                        </p>
                      </div>
                    </div>
                    <div className="ntf-list-groups-single">
                      <div className="ntf-list-groups-icon text-info">
                        <i className="fa-solid fa-circle-dollar-to-slot" />
                      </div>
                      <div className="ntf-list-groups-caption">
                        <p className="small">
                          Your payment for{" "}
                          <strong className="text-danger">Resido</strong>{" "}
                          proerty are cancelled due to server error
                        </p>
                      </div>
                    </div>
                    <div className="ntf-list-groups-single">
                      <a href="#" className="ntf-more">
                        View All Notifications
                      </a>
                    </div>
                  </div>
                </div> */}
              </div>
            </li>
            <li>
              <AccountDropdown />
            </li>
          </>
        ) : (
          <>
            <li className="list-buttons border">
              <a
                onClick={() => auth.showLoginModal(true)}
                className="cursor-pointer"
              >
                <i className="fas fa-sign-in-alt me-2" />
                Sign In
              </a>
            </li>
            <li className="list-buttons ms-2">
              <a
                onClick={() => auth.showRegisterModal(true)}
                className="cursor-pointer"
              >
                <i className="fas fa-user-alt me-2" />
                Sign Up
              </a>
            </li>
          </>
        )}
      </ul>
    </>
  );
};

export default Index;
