import { useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "components/AuthContext";
import ProfileImage from "assets/media/profile-image.jpg";

const AccountDropdown = () => {
  const navigate = useNavigate();
  const auth = useAuth();
  const user = auth.user;
  const userDefaultImage = auth.userDefaultImage;
  const [showUserDrop, setShowUserDrop] = useState(false);
  const wrapperRef = useRef(null);
  const btnRef = useRef(null);

  const logout = () => {
    auth.logout();
  };

  const onImageError = (e) => {
    e.target.src = ProfileImage;
  };

  function showDrop() {
    if (showUserDrop) {
      setShowUserDrop(false);
    } else {
      setShowUserDrop(true);
    }
  }

  function goTo(link) {
    navigate(link);
    setShowUserDrop(false);
  }

  useEffect(() => {
    function handleClickOutside(event) {
      if (
        wrapperRef.current &&
        !wrapperRef.current.contains(event.target) &&
        !btnRef.current.contains(event.target)
      ) {
        if (wrapperRef.current.classList.contains("show")) {
          wrapperRef.current.classList.remove("show");
          setShowUserDrop(false);
        }
      }
    }
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [showUserDrop]);
  return (
    <div className="btn-group account-drop">
      <button
        type="button"
        className="btn btn-order-by-filt"
        onClick={() => showDrop()}
        ref={btnRef}
      >
        <img
          src={userDefaultImage}
          className="img-fluid circle"
          style={{ minHeight: "100%" }}
          onError={onImageError}
          alt=""
        />
      </button>
      <div
        className={
          "dropdown-menu pull-right animated flipInX " +
          (showUserDrop ? "show" : "")
        }
        style={{
          position: "absolute",
          inset: "0px auto auto 0px",
          margin: 0,
          transform: "translate(-162px, 45px)",
        }}
        data-popper-placement="bottom-start"
        ref={wrapperRef}
      >
        <div className="drp_menu_headr">
          <h4>Hi, {user.username}</h4>
          <div className="drp_menu_headr-right">
            <button
              type="button"
              className="btn btn-whites"
              onClick={() => logout()}
            >
              Logout
            </button>
          </div>
        </div>
        <ul>
          <li>
            <a
              onClick={() => goTo("profile/dashboard")}
              className="cursor-pointer"
            >
              <i className="fa fa-tachometer-alt" />
              Dashboard
            </a>
          </li>
          <li>
            <a
              onClick={() => goTo("profile/my-profile")}
              className="cursor-pointer"
            >
              <i className="fa fa-user-tie" />
              My Profile
            </a>
          </li>

          <li>
            <a
              onClick={() => goTo("profile/my-properties")}
              className="cursor-pointer"
            >
              <i className="fa fa-tasks" />
              My Properties
            </a>
          </li>

          <li>
            <a
              onClick={() => goTo("profile/add-property")}
              className="cursor-pointer"
            >
              <i className="fa fa-pen-nib" />
              Submit New Property
            </a>
          </li>
          <li>
            <a
              onClick={() => goTo("profile/my-account")}
              className="cursor-pointer"
            >
              <i className="fa fa-user-tie" />
              My Account
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default AccountDropdown;
