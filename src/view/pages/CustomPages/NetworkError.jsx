const NetworkError = () => {
  return (
    <>
      <div
        className="page-title"
        style={{
          background: "#017efa url(/src/assets/media/page-title.png) no-repeat",
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12">
              <h2 className="ipt-title">Network Error</h2>
              <span className="ipn-subtitle">Here is error page</span>
            </div>
          </div>
        </div>
      </div>
      <section className="error-wrap">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6 col-md-10">
              <div className="text-center">
                <i
                  className="fas fa-exclamation-triangle"
                  style={{ fontSize: "500px" }}
                ></i>
                <h1>Network Error</h1>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default NetworkError;
