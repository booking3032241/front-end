import { useNavigate } from "react-router-dom";

const PageNotFound = () => {
  const navigate = useNavigate();
  return (
    <>
      {/* ============================ Page Title Start================================== */}
      <div
        className="page-title"
        style={{
          background:
            "#017efa url(/src/assets/media//page-title.png) no-repeat",
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12">
              <h2 className="ipt-title">Error Page</h2>
              <span className="ipn-subtitle">Here is error page</span>
            </div>
          </div>
        </div>
      </div>
      {/* ============================ Page Title End ================================== */}
      {/* ============================ User Dashboard ================================== */}
      <section className="error-wrap">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-6 col-md-10">
              <div className="text-center">
                <img
                  src="/src/assets/media/404.png"
                  className="img-fluid"
                  alt=""
                />
                <p>
                  Maecenas quis consequat libero, a feugiat eros. Nunc ut
                  lacinia tortor morbi ultricies laoreet ullamcorper phasellus
                  semper
                </p>
                <a className="btn btn-primary" onClick={() => navigate("/")}>
                  Back To Home
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* ============================ User Dashboard End ================================== */}
    </>
  );
};

export default PageNotFound;
