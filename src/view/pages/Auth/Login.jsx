import { Modal, Form } from "react-bootstrap";
import { Formik } from "formik";
import * as Yup from "yup";
import { useAuth } from "components/AuthContext";
const Login = ({ show, onHide }) => {
  const auth = useAuth();
  const error_Message = "Obligatoire !";
  const login = (fields) => {
    auth.login(fields);
  };
  return (
    <Formik
      initialValues={{
        username: "",
        password: "",
      }}
      validationSchema={Yup.object().shape({
        username: Yup.string().required(`${error_Message}`),
        password: Yup.string().required(`${error_Message}`),
      })}
      onSubmit={(fields) => {
        login(fields);
      }}
    >
      {({ handleSubmit, handleChange, values, touched, errors }) => (
        <Modal
          show={show}
          onHide={onHide}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Sign In
            </Modal.Title>
          </Modal.Header>
          <Form noValidate onSubmit={handleSubmit}>
            <Modal.Body>
              <div className="form-floating mb-4">
                <input
                  name="username"
                  type="text"
                  value={values["username"]}
                  className={
                    "form-control  " +
                    (errors.username && touched.username ? " is-invalid" : "") +
                    (!errors.username && touched.username ? " is-valid" : "")
                  }
                  onChange={(e) => handleChange(e)}
                />
                <label>User Name</label>
              </div>
              {errors.username && (
                <div className="invalid-feedback d-flex mb-3">
                  {errors.username}
                </div>
              )}
              <div className="form-floating mb-4">
                <input
                  name="password"
                  type="password"
                  value={values["password"]}
                  className={
                    "form-control  " +
                    (errors.password && touched.password ? " is-invalid" : "") +
                    (!errors.password && touched.password ? " is-valid" : "")
                  }
                  onChange={(e) => handleChange(e)}
                />
                <label>Password</label>
              </div>
              {errors.password && (
                <div className="invalid-feedback d-flex mb-3">
                  {errors.password}
                </div>
              )}
              <div className="form-group">
                <button
                  type="submit"
                  className="btn btn-primary full-width font--bold btn-lg"
                >
                  Log In
                </button>
              </div>
              <div className="modal-flex-item mb-3">
                <div className="modal-flex-first"></div>
                <div className="modal-flex-last">
                  <a>Forget Password?</a>
                </div>
              </div>
              <div className="social-login">
                <ul>
                  <li>
                    <a className="btn connect-fb">
                      <i className="fa-brands fa-facebook" />
                      Facebook
                    </a>
                  </li>
                  <li>
                    <a className="btn connect-google">
                      <i className="fa-brands fa-google" />
                      Google+
                    </a>
                  </li>
                </ul>
              </div>
              <p className="mt-2">
                Don't have an account yet?
                <a
                  onClick={() => {
                    auth.showLoginModal(false);
                    auth.showRegisterModal(true);
                  }}
                  className="theme-cl font--bold ms-1 cursor-pointer"
                >
                  Sign Up
                </a>
              </p>
            </Modal.Body>
          </Form>
        </Modal>
      )}
    </Formik>
  );
};

export default Login;
