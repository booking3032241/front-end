import { Modal, Form } from "react-bootstrap";
import { Formik } from "formik";
import * as Yup from "yup";
import { useAuth } from "components/AuthContext";
const SingUp = ({ show, onHide }) => {
  const auth = useAuth();
  const error_Message = "Obligatoire !";
  const register = (fields) => {
    fields.phone = fields.phone.toString();
    auth.register(fields);
  };
  return (
    <Formik
      initialValues={{
        firstname: "",
        lastname: "",
        email: "",
        phone: "",
        username: "",
        password: "",
      }}
      validationSchema={Yup.object().shape({
        firstname: Yup.string().required(`${error_Message}`),
        lastname: Yup.string().required(`${error_Message}`),
        username: Yup.string().required(`${error_Message}`),
        password: Yup.string().required(`${error_Message}`),
      })}
      onSubmit={(fields) => {
        register(fields);
      }}
    >
      {({ handleSubmit, handleChange, values, touched, errors }) => (
        <Modal
          show={show}
          onHide={onHide}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Create An Account
            </Modal.Title>
          </Modal.Header>
          <Form noValidate onSubmit={handleSubmit}>
            <Modal.Body>
              <div className="form-floating mb-4">
                <input
                  name="firstname"
                  type="text"
                  value={values["firstname"]}
                  className={
                    "form-control  " +
                    (errors.firstname && touched.firstname
                      ? " is-invalid"
                      : "") +
                    (!errors.firstname && touched.firstname ? " is-valid" : "")
                  }
                  onChange={(e) => handleChange(e)}
                />
                <label>First Name</label>
              </div>
              {errors.firstname && (
                <div className="invalid-feedback d-flex mb-3">
                  {errors.firstname}
                </div>
              )}
              <div className="form-floating mb-4">
                <input
                  name="lastname"
                  type="text"
                  value={values["lastname"]}
                  className={
                    "form-control  " +
                    (errors.lastname && touched.lastname ? " is-invalid" : "") +
                    (!errors.lastname && touched.lastname ? " is-valid" : "")
                  }
                  onChange={(e) => handleChange(e)}
                />
                <label>Last Name</label>
              </div>
              {errors.lastname && (
                <div className="invalid-feedback d-flex mb-3">
                  {errors.lastname}
                </div>
              )}
              <div className="form-floating mb-4">
                <input
                  name="email"
                  type="email"
                  value={values["email"]}
                  className={
                    "form-control  " +
                    (errors.email && touched.email ? " is-invalid" : "") +
                    (!errors.email && touched.email ? " is-valid" : "")
                  }
                  onChange={(e) => handleChange(e)}
                />
                <label>Email</label>
              </div>
              {errors.email && (
                <div className="invalid-feedback d-flex mb-3">
                  {errors.email}
                </div>
              )}
              <div className="form-floating mb-4">
                <input
                  name="phone"
                  type="number"
                  value={values["phone"]}
                  className={
                    "form-control  " +
                    (errors.phone && touched.phone ? " is-invalid" : "") +
                    (!errors.phone && touched.phone ? " is-valid" : "")
                  }
                  onChange={(e) => handleChange(e)}
                />
                <label>Phone</label>
              </div>
              {errors.phone && (
                <div className="invalid-feedback d-flex mb-3">
                  {errors.phone}
                </div>
              )}
              <div className="form-floating mb-4">
                <input
                  name="username"
                  type="text"
                  value={values["username"]}
                  className={
                    "form-control  " +
                    (errors.username && touched.username ? " is-invalid" : "") +
                    (!errors.username && touched.username ? " is-valid" : "")
                  }
                  onChange={(e) => handleChange(e)}
                />
                <label>User Name</label>
              </div>
              {errors.username && (
                <div className="invalid-feedback d-flex mb-3">
                  {errors.username}
                </div>
              )}
              <div className="form-floating mb-4">
                <input
                  name="password"
                  type="password"
                  value={values["password"]}
                  className={
                    "form-control  " +
                    (errors.password && touched.password ? " is-invalid" : "") +
                    (!errors.password && touched.password ? " is-valid" : "")
                  }
                  onChange={(e) => handleChange(e)}
                />
                <label>Password</label>
              </div>
              {errors.password && (
                <div className="invalid-feedback d-flex mb-3">
                  {errors.password}
                </div>
              )}
              <div className="form-group">
                <button
                  type="submit"
                  className="btn btn-primary full-width font--bold btn-lg"
                >
                  Create An Account
                </button>
              </div>
              <div className="social-login">
                <ul>
                  <li>
                    <a className="btn connect-fb">
                      <i className="fa-brands fa-facebook" />
                      Facebook
                    </a>
                  </li>
                  <li>
                    <a className="btn connect-google">
                      <i className="fa-brands fa-google" />
                      Google+
                    </a>
                  </li>
                </ul>
              </div>
              <p className="mt-2">
                Already account exist ?
                <a
                  onClick={() => {
                    auth.showRegisterModal(false);
                    auth.showLoginModal(true);
                  }}
                  className="theme-cl font--bold ms-1 cursor-pointer"
                >
                  Sign In
                </a>
              </p>
            </Modal.Body>
          </Form>
        </Modal>
      )}
    </Formik>
  );
};

export default SingUp;
