import { useState } from "react";
import ProfileImage from "assets/media/profile-image.jpg";
import "./CardProfile.css";
const ImgUpload = ({ onChange, src }) => {
  const onImageError = (e) => {
    e.target.src = ProfileImage;
  };
  return (
    <label htmlFor="photo-upload" className="custom-file-upload fas">
      <div className="img-wrap img-upload">
        <img for="photo-upload" src={src} onError={onImageError} />
      </div>
      <input id="photo-upload" type="file" onChange={onChange} />
    </label>
  );
};

const Edit = ({ onSubmit, children }) => (
  <form onSubmit={onSubmit}>{children}</form>
);

const CardProfile = ({ setFieldValue, imageSrc }) => {
  const [imagePreviewUrl, setImagePreviewUrl] = useState(imageSrc);

  const photoUpload = (e) => {
    e.preventDefault();
    const reader = new FileReader();
    const file = e.target.files[0];
    reader.onloadend = () => {
      setFieldValue("file", file);
      setImagePreviewUrl(reader.result);
    };
    reader.readAsDataURL(file);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  return (
    <div className="custom-picture-upload">
      <Edit onSubmit={handleSubmit}>
        <ImgUpload onChange={photoUpload} src={imagePreviewUrl} />
      </Edit>
    </div>
  );
};

export default CardProfile;
