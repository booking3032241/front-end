import { useEffect, useState } from "react";
import api from "api";
import { alertError, alertSuccess } from "helpers/GeneralFunctions";
import { Formik } from "formik";
import * as Yup from "yup";
import { Form, Modal } from "react-bootstrap";
import { useAuth } from "components/AuthContext";
import CardProfile from "./CardProfile";
import Loading from "components/Loading";

const ChangeProfileImage = ({ fetchOwner, show, onHide }) => {
  const auth = useAuth();
  const userId = auth.user?.id;
  const [btnLoading, setBtnLoading] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [gallery, setGallery] = useState([]);

  function changeGallery(formData, resetForm) {
    if (gallery?.imageSrc) {
      updateGallery(formData, resetForm);
    } else {
      createGallery(formData, resetForm);
    }
  }
  function createGallery(formData, resetForm) {
    setBtnLoading(true);
    api
      .post("galleries", formData)
      .then((response) => {
        alertSuccess("Successefuly added");
        resetForm();
        fetchGallery();
        fetchOwner();
        auth.fetchDefaultUserImage();
        onHide();
        setBtnLoading(false);
      })
      .catch((error) => {
        var message = error.response.data.errors;
        alertError(message);
        resetForm();
        setBtnLoading(false);
      });
  }
  function updateGallery(formData, resetForm) {
    setBtnLoading(true);
    api
      .patch("galleries", formData)
      .then((response) => {
        alertSuccess("Successefuly updated");
        resetForm();
        fetchGallery();
        fetchOwner();
        auth.fetchDefaultUserImage();
        onHide();
        setBtnLoading(false);
      })
      .catch((error) => {
        var message = error.response.data.errors;
        alertError(message);
        resetForm();
        setBtnLoading(false);
      });
  }
  const fetchGallery = () => {
    api
      .get(`galleries/default?entityId=1&entityIdentifier=${userId}`)
      .then((response) => {
        setGallery(response.data);
        setIsLoading(false);
      })
      .catch((error) => {
        var message = error.response.data;
        setIsLoading(false);
      });
  };
  useEffect(() => {
    fetchGallery();
  }, []);
  return (
    <Loading isLoading={isLoading}>
      <Formik
        initialValues={{
          entityId: "1",
          entityIdentifier: userId,
          file: "",
        }}
        validationSchema={Yup.object().shape({
          file: Yup.string().required(`Please check a photo !`),
        })}
        enableReinitialize={true}
        onSubmit={(fields, { resetForm }) => {
          var formData = new FormData();
          if (gallery?.imageSrc) {
            formData.append("id", gallery.id);
          }
          formData.append("entityId", fields.entityId);
          formData.append("entityIdentifier", fields.entityIdentifier);
          formData.append("imageFile", fields.file);

          changeGallery(formData, resetForm);
        }}
      >
        {({ handleSubmit, setFieldValue, values, touched, errors }) => (
          <Modal
            show={show}
            onHide={onHide}
            size="sx"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title id="contained-modal-title-vcenter">
                Change your cover photo
              </Modal.Title>
            </Modal.Header>
            <Form noValidate onSubmit={handleSubmit}>
              <Modal.Body>
                <div className="text-center">
                  <div className="frm_submit_block">
                    <CardProfile
                      setFieldValue={setFieldValue}
                      imageSrc={gallery ? gallery.imageSrc : null}
                    />
                    {errors.file && (
                      <div className="mb-3" style={{ color: "red" }}>
                        {errors.file}
                      </div>
                    )}
                    <div className="form-group">
                      <button className="btn btn-primary" type="submit">
                        {gallery?.imageSrc
                          ? "Update profile picture"
                          : "Add new profile picture"}
                      </button>
                    </div>
                  </div>
                </div>
              </Modal.Body>
            </Form>
          </Modal>
        )}
      </Formik>
    </Loading>
  );
};

export default ChangeProfileImage;
