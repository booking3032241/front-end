import ChangeProfileImage from "./components/ChangeProfileImage";
import { useState } from "react";
import ProfileImage from "assets/media/profile-image.jpg";
const Index = ({ owner, fetchOwner }) => {
  const [showModal, setShowModal] = useState(false);
  const onImageError = (e) => {
    e.target.src = ProfileImage;
  };

  return (
    <div>
      <div className="dash_user_avater">
        <div className="d-flex justify-content-center">
          <div className="custom-box">
            <button
              className="btn btn-info btn-sm btn-rounded relative"
              onClick={() => setShowModal(true)}
            >
              <i className="fa-regular fa-pen-to-square fs-4"></i>
            </button>
            <img
              src={owner.defaultImageSrc}
              onError={onImageError}
              className=""
              alt=""
            />
          </div>
        </div>
        {showModal && (
          <ChangeProfileImage
            show={showModal}
            onHide={() => setShowModal(false)}
            fetchOwner={fetchOwner}
          />
        )}
        <h4>{`${owner.firstname} ${owner.lastname}`} </h4>
        <span className="font--medium small">
          <i className="fa-solid fa-location-dot me-2" />
          {owner.address}
        </span>
      </div>
      <div className="adgt-wriop-social">
        <ul>
          <li>
            <a className="bg--facebook">
              <i className="fa-brands fa-facebook-f" />
            </a>
          </li>
          <li>
            <a className="bg--twitter">
              <i className="fa-brands fa-twitter" />
            </a>
          </li>
          <li>
            <a className="bg--googleplus">
              <i className="fa-brands fa-google-plus-g" />
            </a>
          </li>
          <li>
            <a className="bg--linkedin">
              <i className="fa-brands fa-linkedin-in" />
            </a>
          </li>
        </ul>
      </div>
      <div className="adgt-wriop-footer py-3 px-3">
        <div className="single-button d-flex align-items-center justify-content-between">
          <button
            type="button"
            className="btn btn-md font--bold btn-light-primary me-2 full-width"
          >
            <i className="fa-solid fa-phone me-2" />
            {owner.phone}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Index;
