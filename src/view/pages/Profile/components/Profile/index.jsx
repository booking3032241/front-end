import { useNavigate } from "react-router-dom";
import "./components/style.css";
import Banner from "assets/media/Banner.jpg";
import ProfileImage from "assets/media/profile-image.jpg";

const Index = ({ owner }) => {
  const navigate = useNavigate();
  const onImageError = (e) => {
    e.target.src = ProfileImage;
  };
  return (
    <div className="adgt-wriop-block border rounded">
      <div className="">
        <div className="">
          {" "}
          <div>
            <img className="card-img-top" src={Banner} alt="" />
          </div>
          <div className=" little-profile text-center">
            <div className="pro-img">
              <img
                src={owner.defaultImageSrc}
                alt="user"
                onError={onImageError}
              />
            </div>
          </div>
        </div>
      </div>

      <div className="adgt-wriop-caption px-3 py-4">
        <div className="vl-elfo-group">
          <div className="vl-elfo-icon">
            <i className="fa-solid fa-phone-volume" />
          </div>
          <div className="vl-elfo-caption">
            <h6>Call Us</h6>
            <p>{owner.phone}</p>
          </div>
        </div>
        <div className="vl-elfo-group">
          <div className="vl-elfo-icon">
            <i className="fa-regular fa-envelope" />
          </div>
          <div className="vl-elfo-caption">
            <h6>Drop A Mail</h6>
            <p>{owner.email}</p>
          </div>
        </div>

        <div className="vl-elfo-group">
          <div className="vl-elfo-icon">
            <i className="fas fa-map-marker-alt"></i>
          </div>
          <div className="vl-elfo-caption">
            <h6>Address</h6>
            <p>{owner.address}</p>
          </div>
        </div>
        <div className="vl-elfo-group">
          <div className="vl-elfo-icon">
            <i className="fas fa-map-marked-alt"></i>{" "}
          </div>
          <div className="vl-elfo-caption">
            <h6>City</h6>
            <p>{owner.city}</p>
          </div>
        </div>
      </div>
      <div className="adgt-wriop-footer py-3 px-3">
        <div className="single-button d-flex align-items-center justify-content-between">
          <button
            type="button"
            className="btn btn-md font--bold btn-light-primary me-2 full-width"
          >
            <i className="fa-solid fa-phone me-2" />
            {owner.phone}
          </button>
          <button
            type="button"
            className="btn btn-md font--bold btn-light-success me-2 full-width"
            onClick={() => navigate("/profile/my-profile/edit")}
          >
            <i className="fa-solid fa-pencil me-2" />
            Edit
          </button>
        </div>
      </div>
    </div>
  );
};

export default Index;
