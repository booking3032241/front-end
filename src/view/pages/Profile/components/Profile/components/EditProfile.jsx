import { useState } from "react";
import api from "api";
import { alertError, alertSuccess } from "helpers/GeneralFunctions";
import { Formik } from "formik";
import * as Yup from "yup";
import { Form } from "react-bootstrap";
import { useAuth } from "components/AuthContext";
import { useNavigate } from "react-router-dom";

const EditProfile = ({ owner, fetchOwner }) => {
  const navigate = useNavigate();
  const auth = useAuth();
  const userId = auth.user ? auth.user.id : null;
  const [btnUpdateLoading, setBtnUpdateLoading] = useState(false);

  const error_Message = "Obligatoire !";

  const updateOwner = (fields, resetForm) => {
    setBtnUpdateLoading(true);
    api
      .patch("owners", { ...fields })
      .then((response) => {
        alertSuccess("Successfully update");
        resetForm();
        fetchOwner();
        navigate("/profile/my-profile");
        setBtnUpdateLoading(false);
      })
      .catch((error) => {
        var message = error.response.data;
        alertError(message);
        setBtnUpdateLoading(false);
      });
  };
  return (
    <div className="dashboard-body">
      <div className="row">
        <div className="col-lg-12 col-md-12">
          <div className="_prt_filt_dash">
            <div className="_prt_filt_dash_flex">
              <div>
                <h2>Edit profile </h2>
              </div>
            </div>
            <div className="_prt_filt_dash_last m2_hide">
              <div className="_prt_filt_radius"></div>
              <div className="_prt_filt_add_new">
                <a
                  onClick={() => navigate("/profile/my-profile")}
                  className="btn btn-info mr-2 cursor-pointer"
                >
                  <i className="fas fa-eye mr-2"></i>
                  <span className="d-none d-lg-block d-md-block">
                    View profile
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="dashboard-wraper">
        <div className="frm_submit_block">
          <h4>My Profile</h4>
          <div className="frm_submit_wrap">
            <Formik
              initialValues={{
                id: owner.id,
                firstname: owner.firstname,
                lastname: owner.lastname,
                email: owner.email,
                phone: owner.phone,
                address: owner.address,
                city: owner.city,
                state: owner.state,
                zipcode: owner.zipcode,
                description: owner.description,
              }}
              validationSchema={Yup.object().shape({})}
              enableReinitialize={true}
              onSubmit={(fields, { resetForm }) => {
                fields.phone = fields.phone.toString();
                updateOwner(fields, resetForm);
              }}
            >
              {({ handleSubmit, handleChange, values, touched, errors }) => (
                <Form noValidate onSubmit={handleSubmit}>
                  <div className="row">
                    <div className="form-group col-md-6">
                      <label>First Name</label>
                      <input
                        name="firstname"
                        type="text"
                        value={values["firstname"]}
                        className={
                          "form-control  " +
                          (errors.firstname && touched.firstname
                            ? " is-invalid"
                            : "") +
                          (!errors.firstname && touched.firstname
                            ? " is-valid"
                            : "")
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </div>
                    {errors.firstname && (
                      <div className="invalid-feedback d-flex mb-3">
                        {errors.firstname}
                      </div>
                    )}
                    <div className="form-group col-md-6">
                      <label>Last Name</label>
                      <input
                        name="lastname"
                        type="text"
                        value={values["lastname"]}
                        className={
                          "form-control  " +
                          (errors.lastname && touched.lastname
                            ? " is-invalid"
                            : "") +
                          (!errors.lastname && touched.lastname
                            ? " is-valid"
                            : "")
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </div>
                    {errors.lastname && (
                      <div className="invalid-feedback d-flex mb-3">
                        {errors.lastname}
                      </div>
                    )}
                    <div className="form-group col-md-6">
                      <label>Email</label>
                      <input
                        name="email"
                        type="email"
                        value={values["email"]}
                        className={
                          "form-control  " +
                          (errors.email && touched.email ? " is-invalid" : "") +
                          (!errors.email && touched.email ? " is-valid" : "")
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </div>
                    {errors.email && (
                      <div className="invalid-feedback d-flex mb-3">
                        {errors.email}
                      </div>
                    )}

                    <div className="form-group col-md-6">
                      <label>Phone</label>
                      <input
                        name="phone"
                        type="number"
                        value={values["phone"]}
                        className={
                          "form-control  " +
                          (errors.phone && touched.phone ? " is-invalid" : "") +
                          (!errors.phone && touched.phone ? " is-valid" : "")
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </div>
                    {errors.phone && (
                      <div className="invalid-feedback d-flex mb-3">
                        {errors.phone}
                      </div>
                    )}
                    <div className="form-group col-md-6">
                      <label>Address</label>
                      <input
                        name="address"
                        type="text"
                        value={values["address"]}
                        className={
                          "form-control  " +
                          (errors.address && touched.address
                            ? " is-invalid"
                            : "") +
                          (!errors.address && touched.address
                            ? " is-valid"
                            : "")
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </div>
                    {errors.address && (
                      <div className="invalid-feedback d-flex mb-3">
                        {errors.address}
                      </div>
                    )}
                    <div className="form-group col-md-6">
                      <label>City</label>
                      <input
                        name="city"
                        type="text"
                        value={values["city"]}
                        className={
                          "form-control  " +
                          (errors.city && touched.city ? " is-invalid" : "") +
                          (!errors.city && touched.city ? " is-valid" : "")
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </div>
                    {errors.city && (
                      <div className="invalid-feedback d-flex mb-3">
                        {errors.city}
                      </div>
                    )}
                    <div className="form-group col-md-6">
                      <label>State</label>
                      <input
                        name="state"
                        type="text"
                        value={values["state"]}
                        className={
                          "form-control  " +
                          (errors.state && touched.state ? " is-invalid" : "") +
                          (!errors.state && touched.state ? " is-valid" : "")
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </div>
                    {errors.state && (
                      <div className="invalid-feedback d-flex mb-3">
                        {errors.state}
                      </div>
                    )}
                    <div className="form-group col-md-6">
                      <label>Zip</label>
                      <input
                        name="zipcode"
                        type="text"
                        value={values["zipcode"]}
                        className={
                          "form-control  " +
                          (errors.zipcode && touched.zipcode
                            ? " is-invalid"
                            : "") +
                          (!errors.zipcode && touched.zipcode
                            ? " is-valid"
                            : "")
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </div>
                    {errors.zipcode && (
                      <div className="invalid-feedback d-flex mb-3">
                        {errors.zipcode}
                      </div>
                    )}
                    <div className="form-group col-md-12">
                      <label>Description</label>
                      <textarea
                        name="description"
                        value={values["description"]}
                        className={
                          "form-control  " +
                          (errors.description && touched.description
                            ? " is-invalid"
                            : "") +
                          (!errors.description && touched.description
                            ? " is-valid"
                            : "")
                        }
                        onChange={(e) => handleChange(e)}
                      />
                    </div>
                    {errors.description && (
                      <div className="invalid-feedback d-flex mb-3">
                        {errors.description}
                      </div>
                    )}
                    <div className="form-group col-lg-12 col-md-12 mt-4 text-right">
                      <button className="btn btn-theme btn-lg" type="submit">
                        Save Changes
                      </button>
                    </div>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditProfile;
