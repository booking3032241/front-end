import { useNavigate, useParams } from "react-router-dom";
import api from "api";
import { useEffect, useState } from "react";
import _ from "lodash";
import Loading from "components/Loading";
import CustomSlider from "components/CustomSlider";
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";
import MapPosition from "../../../../../../components/MapPosition";

const ViewProperty = () => {
  const navigate = useNavigate();
  const params = useParams();
  const propertyId = params.propertyId;
  const [property, setProperty] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const fetchProperty = () => {
    api
      .get(`estates/${propertyId}`)
      .then((response) => {
        setProperty(response.data);
        setIsLoading(false);
      })
      .catch((error) => {
        var message = error.response.data;
        setIsLoading(false);
      });
  };
  useEffect(() => {
    fetchProperty();
  }, []);
  return (
    <>
      <div className="row">
        <div className="col-lg-12 col-md-12">
          <div className="_prt_filt_dash">
            <div className="_prt_filt_dash_flex">
              <div>
                <h2>Property #{property.id} </h2>
              </div>
            </div>
            <div className="_prt_filt_dash_last m2_hide">
              <div className="_prt_filt_radius"></div>
              <div className="_prt_filt_add_new">
                <a
                  onClick={() => navigate("/profile/my-properties")}
                  className="btn btn-primary mr-2 cursor-pointer"
                >
                  <i className="fas fa-arrow-left mr-2"></i>
                  <span className="d-none d-lg-block d-md-block">
                    Go to my property list
                  </span>
                </a>
                <a
                  onClick={() =>
                    navigate("/profile/edit-property/" + property.id)
                  }
                  className="btn btn-warning mr-2 cursor-pointer"
                >
                  <i className="fas fa-edit mr-2"></i>
                  <span className="d-none d-lg-block d-md-block">
                    Edit property
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="featured_slick_gallery gray">
        <div className="featured_slick_gallery-slide">
          <Loading isLoading={isLoading}>
            {!_.isEmpty(property?.galleries) && (
              <CustomSlider data={property.galleries}>
                {property.galleries.map((item, key) => {
                  return (
                    <div key={key} className="featured_slick_padd mfp-gallery">
                      <img
                        src={item.imageSrc}
                        className="img-fluid mx-auto"
                        style={{ height: "496px", width: "100%" }}
                        alt=""
                      />
                    </div>
                  );
                })}
              </CustomSlider>
            )}
          </Loading>
        </div>
      </div>
      {/* ============================ Hero Banner End ================================== */}
      {/* ============================ Property Detail Start ================================== */}
      <section className="gray-simple pt-5">
        <div className="container">
          {/* Main Info Detail */}
          <div className="vesh-detail-bloc">
            <div className="vesh-detail-headup">
              <div className="vesh-detail-headup-first">
                <div className="prt-detail-title-desc">
                  <span className="label label-success">For Sale</span>
                  <h4>{property.name}</h4>
                  <span className="text-mid">
                    <i className="fa-solid fa-location-dot me-2" />
                    {property.address}
                  </span>
                  <div className="list-fx-features mt-2">
                    <div className="list-fx-fisrt">
                      <span className="label font--medium label-light-success me-2">
                        {property.bedrooms} Beds
                      </span>
                      <span className="label font--medium label-light-info me-2">
                        {property.bathrooms} Bath
                      </span>
                      <span className=" label font--medium label-light-danger">
                        {property.space} Sqft
                      </span>
                    </div>
                    <div className="list-fx-last"></div>
                  </div>
                </div>
              </div>
              <div className="vesh-detail-headup-last">
                <h3 className="prt-price-fix theme-cl">
                  ${property.price}
                  <span>One Time</span>
                </h3>
              </div>
            </div>
          </div>
          {/* About Property Detail */}
          <div className="vesh-detail-bloc">
            <div className="vesh-detail-bloc_header">
              <h4 className="property_block_title no-arrow">About Property</h4>
            </div>
            <div className="vesh-detail-bloc-body">
              <div className="row g-3">
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <p>{property.description}</p>
                </div>
              </div>
            </div>
          </div>
          {/* Basic Detail */}
          <div className="vesh-detail-bloc">
            <div className="vesh-detail-bloc_header">
              <a
                data-bs-toggle="collapse"
                data-parent="#basicinfo"
                data-bs-target="#basicinfo"
                aria-controls="basicinfo"
                aria-expanded="false"
              >
                <h4 className="property_block_title">Basic Detail</h4>
              </a>
            </div>
            <div
              id="basicinfo"
              className="panel-collapse collapse show"
              aria-labelledby="basicinfo"
            >
              <div className="vesh-detail-bloc-body">
                <div className="row g-3">
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-solid fa-bed" />
                      </div>
                      <div className="ilio-text">3 Bedrooms</div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-solid fa-bath" />
                      </div>
                      <div className="ilio-text">2 Bathrooms</div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-solid fa-layer-group" />
                      </div>
                      <div className="ilio-text">4,240 sq ft</div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-solid fa-warehouse" />
                      </div>
                      <div className="ilio-text">1 Garage</div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-regular fa-building" />
                      </div>
                      <div className="ilio-text">Apartment</div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-solid fa-building-wheat" />
                      </div>
                      <div className="ilio-text">Built 1982</div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-solid fa-building-circle-check" />
                      </div>
                      <div className="ilio-text">Active</div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-solid fa-fan" />
                      </div>
                      <div className="ilio-text">Central A/C</div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-regular fa-snowflake" />
                      </div>
                      <div className="ilio-text">Forced Air</div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-solid fa-bowl-food" />
                      </div>
                      <div className="ilio-text">Kitchen Facilities</div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-solid fa-martini-glass-citrus" />
                      </div>
                      <div className="ilio-text">Bar &amp; Drinks</div>
                    </div>
                  </div>
                  <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                    <div className="ilio-icon-wrap">
                      <div className="ilio-icon">
                        <i className="fa-regular fa-building" />
                      </div>
                      <div className="ilio-text">4 Floor</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="vesh-detail-bloc">
            <div className="vesh-detail-bloc_header">
              <h4 className="property_block_title no-arrow">Location</h4>
            </div>
            <div className="vesh-detail-bloc-body">
              <MapPosition lat={property.latitude} lng={property.longitude} />
            </div>
          </div>
        </div>
      </section>
      {/* ============================ Property Detail End ================================== */}
    </>
  );
};

export default ViewProperty;
