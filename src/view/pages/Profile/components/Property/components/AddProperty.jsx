import api from "api";
import { alertError, alertSuccess } from "helpers/GeneralFunctions";
import { Formik } from "formik";
import * as Yup from "yup";
import { Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Map from "components/Map";
import LocationForm from "./LocationForm";

const AddProperty = ({ ownerId }) => {
  const navigate = useNavigate();

  const addProperty = (fields, resetForm) => {
    api
      .post("estates", fields)
      .then((response) => {
        alertSuccess("Successfully added");
        resetForm();
        navigate("/profile/my-properties");
      })
      .catch((error) => {
        var message = error.response.data;
        alertError(message);
      });
  };

  return (
    <div className="dashboard-body">
      <div className="dashboard-wraper">
        <div className="row">
          {/* Submit Form */}
          <div className="col-lg-12 col-md-12">
            <div className="submit-page">
              <Formik
                initialValues={{
                  name: "",
                  ownerId: ownerId,
                  description: "",
                  nature: "",
                  ptype: "",
                  price: "",
                  space: "",
                  bedrooms: "",
                  bathrooms: "",
                  address: "",
                  country: "",
                  city: "",
                  state: "",
                  zipcode: "",
                  latitude: "",
                  longitude: "",
                }}
                validationSchema={Yup.object().shape({})}
                // enableReinitialize={true}
                onSubmit={(fields, { resetForm }) => {
                  fields.ownerId = fields.ownerId.toString();
                  fields.latitude = fields.latitude.toString();
                  fields.longitude = fields.longitude.toString();
                  addProperty(fields, resetForm);
                }}
              >
                {({
                  handleSubmit,
                  handleChange,
                  setFieldValue,
                  values,
                  touched,
                  errors,
                }) => (
                  <Form noValidate onSubmit={handleSubmit}>
                    {/* Basic Information */}
                    <div className="frm_submit_block">
                      <h3>Basic Information</h3>
                      <div className="frm_submit_wrap">
                        <div className="row">
                          <div className="form-group col-md-12">
                            <label>
                              Property Title
                              <a
                                href="#"
                                className="tip-topdata"
                                data-tip="Property Title"
                              >
                                <i className="fa-solid fa-info" />
                              </a>
                            </label>
                            <input
                              name="name"
                              type="text"
                              value={values["name"]}
                              className={
                                "form-control  " +
                                (errors.name && touched.name
                                  ? " is-invalid"
                                  : "") +
                                (!errors.name && touched.name
                                  ? " is-valid"
                                  : "")
                              }
                              onChange={(e) => handleChange(e)}
                            />
                          </div>
                          {errors.name && (
                            <div className="invalid-feedback d-flex mb-3">
                              {errors.name}
                            </div>
                          )}
                          <div className="form-group col-md-12">
                            <label>Description</label>
                            <textarea
                              name="description"
                              value={values["description"]}
                              className={
                                "form-control  " +
                                (errors.description && touched.description
                                  ? " is-invalid"
                                  : "") +
                                (!errors.description && touched.description
                                  ? " is-valid"
                                  : "")
                              }
                              onChange={(e) => handleChange(e)}
                            />
                          </div>
                          {errors.description && (
                            <div className="invalid-feedback d-flex mb-3">
                              {errors.description}
                            </div>
                          )}
                          <div className="form-group col-md-6">
                            <label>Nature</label>
                            <select
                              name="nature"
                              value={values["nature"]}
                              className={
                                "form-control  " +
                                (errors.nature && touched.nature
                                  ? " is-invalid"
                                  : "") +
                                (!errors.nature && touched.nature
                                  ? " is-valid"
                                  : "")
                              }
                              onChange={(e) => handleChange(e)}
                            >
                              <option value="">&nbsp;</option>
                              <option value={1}>For Rent</option>
                              <option value={2}>For Sale</option>
                            </select>
                          </div>
                          {errors.nature && (
                            <div className="invalid-feedback d-flex mb-3">
                              {errors.nature}
                            </div>
                          )}
                          <div className="form-group col-md-6">
                            <label>Property Type</label>
                            <select
                              name="ptype"
                              value={values["ptype"]}
                              className={
                                "form-control  " +
                                (errors.ptype && touched.ptype
                                  ? " is-invalid"
                                  : "") +
                                (!errors.ptype && touched.ptype
                                  ? " is-valid"
                                  : "")
                              }
                              onChange={(e) => handleChange(e)}
                            >
                              <option value="">&nbsp;</option>
                              <option value={1}>Houses</option>
                              <option value={2}>Apartment</option>
                              <option value={3}>Villas</option>
                              <option value={4}>Commercial</option>
                              <option value={5}>Offices</option>
                              <option value={6}>Garage</option>
                            </select>
                          </div>
                          {errors.ptype && (
                            <div className="invalid-feedback d-flex mb-3">
                              {errors.ptype}
                            </div>
                          )}
                          <div className="form-group col-md-6">
                            <label>Price</label>
                            <input
                              name="price"
                              type="text"
                              value={values["price"]}
                              className={
                                "form-control  " +
                                (errors.price && touched.price
                                  ? " is-invalid"
                                  : "") +
                                (!errors.price && touched.price
                                  ? " is-valid"
                                  : "")
                              }
                              onChange={(e) => handleChange(e)}
                            />
                          </div>
                          {errors.price && (
                            <div className="invalid-feedback d-flex mb-3">
                              {errors.price}
                            </div>
                          )}
                          <div className="form-group col-md-6">
                            <label>Space</label>
                            <input
                              name="space"
                              type="text"
                              value={values["space"]}
                              className={
                                "form-control  " +
                                (errors.space && touched.space
                                  ? " is-invalid"
                                  : "") +
                                (!errors.space && touched.space
                                  ? " is-valid"
                                  : "")
                              }
                              onChange={(e) => handleChange(e)}
                            />
                          </div>
                          {errors.space && (
                            <div className="invalid-feedback d-flex mb-3">
                              {errors.space}
                            </div>
                          )}
                          <div className="form-group col-md-6">
                            <label>Bedrooms</label>
                            <input
                              name="bedrooms"
                              type="text"
                              value={values["bedrooms"]}
                              className={
                                "form-control  " +
                                (errors.bedrooms && touched.bedrooms
                                  ? " is-invalid"
                                  : "") +
                                (!errors.bedrooms && touched.bedrooms
                                  ? " is-valid"
                                  : "")
                              }
                              onChange={(e) => handleChange(e)}
                            />
                          </div>
                          {errors.bedrooms && (
                            <div className="invalid-feedback d-flex mb-3">
                              {errors.bedrooms}
                            </div>
                          )}
                          <div className="form-group col-md-6">
                            <label>Bathrooms</label>
                            <input
                              name="bathrooms"
                              type="text"
                              value={values["bathrooms"]}
                              className={
                                "form-control  " +
                                (errors.bathrooms && touched.bathrooms
                                  ? " is-invalid"
                                  : "") +
                                (!errors.bathrooms && touched.bathrooms
                                  ? " is-valid"
                                  : "")
                              }
                              onChange={(e) => handleChange(e)}
                            />
                          </div>
                          {errors.bathrooms && (
                            <div className="invalid-feedback d-flex mb-3">
                              {errors.bathrooms}
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                    <div className="frm_submit_block">
                      <h3>Location</h3>
                      <div className="frm_submit_wrap">
                        <Map lat={values["latitude"]} lng={values["longitude"]}>
                          <LocationForm
                            handleChange={handleChange}
                            setFieldValue={setFieldValue}
                            values={values}
                            touched={touched}
                            errors={errors}
                          />
                        </Map>
                      </div>
                    </div>
                    <div className="form-group text-right col-lg-12 col-md-12">
                      <button className="btn btn-primary" type="submit">
                        Submit
                      </button>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
      {/* row */}
    </div>
  );
};

export default AddProperty;
