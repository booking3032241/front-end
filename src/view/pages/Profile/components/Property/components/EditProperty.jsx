import { useEffect, useState } from "react";
import api from "api";
import { alertError, alertSuccess } from "helpers/GeneralFunctions";
import { Formik } from "formik";
import * as Yup from "yup";
import { Card, Form } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import EmptyImage from "assets/media/EmptyImage.png";
import Map from "components/Map";
import LocationForm from "./LocationForm";

const AddProperty = () => {
  const navigate = useNavigate();
  const params = useParams();
  const propertyId = params.propertyId;
  const [property, setProperty] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [btnUpdateLoading, setBtnUpdateLoading] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [galleries, setGalleries] = useState([]);

  const fetchProperty = () => {
    api
      .get(`estates/${propertyId}`)
      .then((response) => {
        setProperty(response.data);
      })
      .catch(console.error);
  };
  const updateProperty = (fields, resetForm) => {
    setBtnUpdateLoading(true);
    api
      .patch("estates", { ...fields })
      .then((response) => {
        alertSuccess("Successfully update");
        resetForm();
        navigate("/profile/my-property/" + propertyId);
        setBtnUpdateLoading(false);
      })
      .catch((error) => {
        var message = error.response.data;
        alertError(message);
        setBtnUpdateLoading(false);
      });
  };
  function createGalleries(formData, resetForm) {
    setBtnLoading(true);
    api
      .post("galleries", formData)
      .then((response) => {
        fetchGalleries();
        resetForm();
        setBtnLoading(false);
      })
      .catch((error) => {
        var message = error.response.data.message;
        alertError(message, "error");
        setBtnLoading(false);
      });
  }
  function deleteGallery(id) {
    setBtnLoading(true);
    api
      .delete("galleries/" + id)
      .then((response) => {
        fetchGalleries();
      })
      .catch((error) => {
        var message = error.response.data.message;
        alertError(message, "error");
      });
  }
  const fetchGalleries = () => {
    api
      .get(`galleries?entityId=2&entityIdentifier=${propertyId}`)
      .then((response) => {
        setGalleries(response.data);
        setIsLoading(false);
      })
      .catch((error) => {
        var message = error.response.data;
        setIsLoading(false);
      });
  };
  const confirmDelete = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        deleteGallery(id);
        Swal.fire("Deleted!", "Your photo has been deleted.", "success");
      }
    });
  };
  const onImageError = (e) => {
    e.target.src = EmptyImage;
  };
  useEffect(() => {
    fetchProperty();
    fetchGalleries();
  }, []);
  return (
    <div className="dashboard-body">
      <div className="row">
        <div className="col-lg-12 col-md-12">
          <div className="_prt_filt_dash">
            <div className="_prt_filt_dash_flex">
              <div>
                <h2>Edit Property #{property.id} </h2>
              </div>
            </div>
            <div className="_prt_filt_dash_last m2_hide">
              <div className="_prt_filt_radius"></div>
              <div className="_prt_filt_add_new">
                <a
                  onClick={() => navigate("/profile/my-properties")}
                  className="btn btn-primary mr-2 cursor-pointer"
                >
                  <i className="fas fa-arrow-left mr-2"></i>
                  <span className="d-none d-lg-block d-md-block">
                    Go to my property list
                  </span>
                </a>
                <a
                  onClick={() =>
                    navigate("/profile/my-property/" + property.id)
                  }
                  className="btn btn-info mr-2 cursor-pointer"
                >
                  <i className="fas fa-eye mr-2"></i>
                  <span className="d-none d-lg-block d-md-block">
                    View property
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="dashboard-wraper">
        <div className="row">
          {/* Submit Form */}
          <div className="col-lg-12 col-md-12">
            <div className="submit-page">
              {isLoading ? (
                <div id="preloader">
                  <div className="preloader">
                    <span></span>
                    <span></span>
                  </div>
                </div>
              ) : (
                <>
                  <Formik
                    initialValues={{
                      id: property ? property.id : "",
                      name: property ? property.name : "",
                      ownerId: property ? property.ownerId : "",
                      description: property ? property.description : "",
                      nature: property ? property.nature : "",
                      ptype: property ? property.ptype : "",
                      price: property ? property.price : "",
                      space: property ? property.space : "",
                      bedrooms: property ? property.bedrooms : "",
                      bathrooms: property ? property.bathrooms : "",
                      garages: property ? property.garages : "",
                      address: property ? property.address : "",
                      country: property ? property.country : "",
                      city: property ? property.city : "",
                      state: property ? property.state : "",
                      zipcode: property ? property.zipcode : "",
                      latitude: property ? property.latitude : "",
                      longitude: property ? property.longitude : "",
                    }}
                    validationSchema={Yup.object().shape({})}
                    enableReinitialize={true}
                    onSubmit={(fields, { resetForm }) => {
                      fields.latitude = fields.latitude.toString();
                      fields.longitude = fields.longitude.toString();
                      fields.ownerId = fields.ownerId.toString();
                      updateProperty(fields, resetForm);
                    }}
                  >
                    {({
                      handleSubmit,
                      handleChange,
                      setFieldValue,
                      values,
                      touched,
                      errors,
                    }) => (
                      <Form noValidate onSubmit={handleSubmit}>
                        {/* Basic Information */}
                        <div className="frm_submit_block">
                          <h3>Basic Information</h3>
                          <div className="frm_submit_wrap">
                            <div className="row">
                              <div className="form-group col-md-12">
                                <label>
                                  Property Title
                                  <a
                                    href="#"
                                    className="tip-topdata"
                                    data-tip="Property Title"
                                  >
                                    <i className="fa-solid fa-info" />
                                  </a>
                                </label>
                                <input
                                  name="name"
                                  type="text"
                                  value={values["name"]}
                                  className={
                                    "form-control  " +
                                    (errors.name && touched.name
                                      ? " is-invalid"
                                      : "") +
                                    (!errors.name && touched.name
                                      ? " is-valid"
                                      : "")
                                  }
                                  onChange={(e) => handleChange(e)}
                                />
                              </div>
                              {errors.name && (
                                <div className="invalid-feedback d-flex mb-3">
                                  {errors.name}
                                </div>
                              )}
                              <div className="form-group col-md-12">
                                <label>Description</label>
                                <textarea
                                  name="description"
                                  value={values["description"]}
                                  className={
                                    "form-control  " +
                                    (errors.description && touched.description
                                      ? " is-invalid"
                                      : "") +
                                    (!errors.description && touched.description
                                      ? " is-valid"
                                      : "")
                                  }
                                  onChange={(e) => handleChange(e)}
                                />
                              </div>
                              {errors.description && (
                                <div className="invalid-feedback d-flex mb-3">
                                  {errors.description}
                                </div>
                              )}
                              <div className="form-group col-md-6">
                                <label>Nature</label>
                                <select
                                  name="nature"
                                  value={values["nature"]}
                                  className={
                                    "form-control  " +
                                    (errors.nature && touched.nature
                                      ? " is-invalid"
                                      : "") +
                                    (!errors.nature && touched.nature
                                      ? " is-valid"
                                      : "")
                                  }
                                  onChange={(e) => handleChange(e)}
                                >
                                  <option value="">&nbsp;</option>
                                  <option value={1}>For Rent</option>
                                  <option value={2}>For Sale</option>
                                </select>
                              </div>
                              {errors.nature && (
                                <div className="invalid-feedback d-flex mb-3">
                                  {errors.nature}
                                </div>
                              )}
                              <div className="form-group col-md-6">
                                <label>Property Type</label>
                                <select
                                  name="ptype"
                                  value={values["ptype"]}
                                  className={
                                    "form-control  " +
                                    (errors.ptype && touched.ptype
                                      ? " is-invalid"
                                      : "") +
                                    (!errors.ptype && touched.ptype
                                      ? " is-valid"
                                      : "")
                                  }
                                  onChange={(e) => handleChange(e)}
                                >
                                  <option value="">&nbsp;</option>
                                  <option value={1}>Houses</option>
                                  <option value={2}>Apartment</option>
                                  <option value={3}>Villas</option>
                                  <option value={4}>Commercial</option>
                                  <option value={5}>Offices</option>
                                  <option value={6}>Garage</option>
                                </select>
                              </div>
                              {errors.ptype && (
                                <div className="invalid-feedback d-flex mb-3">
                                  {errors.ptype}
                                </div>
                              )}
                              <div className="form-group col-md-6">
                                <label>Price</label>
                                <input
                                  name="price"
                                  type="text"
                                  value={values["price"]}
                                  className={
                                    "form-control  " +
                                    (errors.price && touched.price
                                      ? " is-invalid"
                                      : "") +
                                    (!errors.price && touched.price
                                      ? " is-valid"
                                      : "")
                                  }
                                  onChange={(e) => handleChange(e)}
                                />
                              </div>
                              {errors.price && (
                                <div className="invalid-feedback d-flex mb-3">
                                  {errors.price}
                                </div>
                              )}
                              <div className="form-group col-md-6">
                                <label>Space</label>
                                <input
                                  name="space"
                                  type="text"
                                  value={values["space"]}
                                  className={
                                    "form-control  " +
                                    (errors.space && touched.space
                                      ? " is-invalid"
                                      : "") +
                                    (!errors.space && touched.space
                                      ? " is-valid"
                                      : "")
                                  }
                                  onChange={(e) => handleChange(e)}
                                />
                              </div>
                              {errors.space && (
                                <div className="invalid-feedback d-flex mb-3">
                                  {errors.space}
                                </div>
                              )}
                              <div className="form-group col-md-6">
                                <label>Bedrooms</label>
                                <input
                                  name="bedrooms"
                                  type="text"
                                  value={values["bedrooms"]}
                                  className={
                                    "form-control  " +
                                    (errors.bedrooms && touched.bedrooms
                                      ? " is-invalid"
                                      : "") +
                                    (!errors.bedrooms && touched.bedrooms
                                      ? " is-valid"
                                      : "")
                                  }
                                  onChange={(e) => handleChange(e)}
                                />
                              </div>
                              {errors.bedrooms && (
                                <div className="invalid-feedback d-flex mb-3">
                                  {errors.bedrooms}
                                </div>
                              )}
                              <div className="form-group col-md-6">
                                <label>Bathrooms</label>
                                <input
                                  name="bathrooms"
                                  type="text"
                                  value={values["bathrooms"]}
                                  className={
                                    "form-control  " +
                                    (errors.bathrooms && touched.bathrooms
                                      ? " is-invalid"
                                      : "") +
                                    (!errors.bathrooms && touched.bathrooms
                                      ? " is-valid"
                                      : "")
                                  }
                                  onChange={(e) => handleChange(e)}
                                />
                              </div>
                              {errors.bathrooms && (
                                <div className="invalid-feedback d-flex mb-3">
                                  {errors.bathrooms}
                                </div>
                              )}
                            </div>
                          </div>
                        </div>

                        <div className="frm_submit_block">
                          <h3>Location</h3>
                          <div className="frm_submit_wrap">
                            <Map
                              lat={values["latitude"]}
                              lng={values["longitude"]}
                            >
                              <LocationForm
                                handleChange={handleChange}
                                setFieldValue={setFieldValue}
                                values={values}
                                touched={touched}
                                errors={errors}
                              />
                            </Map>
                          </div>
                        </div>
                        <div className="form-group text-right col-lg-12 col-md-12">
                          <button className="btn btn-primary" type="submit">
                            Submit
                          </button>
                        </div>
                      </Form>
                    )}
                  </Formik>
                  <Formik
                    initialValues={{
                      entityId: "2",
                      entityIdentifier: propertyId,
                      file: "",
                    }}
                    validationSchema={Yup.object().shape({
                      file: Yup.string().required(`Please check a photo !`),
                    })}
                    onSubmit={(fields, { resetForm }) => {
                      var formData = new FormData();
                      formData.append("name", fields.name);
                      formData.append("entityId", fields.entityId);
                      formData.append(
                        "entityIdentifier",
                        fields.entityIdentifier
                      );
                      formData.append("imageFile", fields.file);
                      formData.append("imageName", "aaaaa");
                      formData.append("imageSrc", "bbbbbbbb");

                      createGalleries(formData, resetForm);
                    }}
                  >
                    {({ handleSubmit, setFieldValue, errors }) => (
                      <Form noValidate onSubmit={handleSubmit}>
                        {/* Basic Information */}
                        <div className="frm_submit_block mb-5">
                          <h3>Galleries</h3>
                          <div className="frm_submit_wrap">
                            <div className="row">
                              <div className="form-group col-md-6">
                                <label htmlFor="file">File</label>
                                <input
                                  name="file"
                                  type="file"
                                  onChange={(event) => {
                                    setFieldValue(
                                      "file",
                                      event.currentTarget.files[0]
                                    );
                                  }}
                                  className="form-control"
                                />
                                {errors.file && (
                                  <div className="invalid-feedback d-flex mb-3 mt3">
                                    {errors.file}
                                  </div>
                                )}
                              </div>
                              <div className="col-md-4 d-flex align-items-center justify-content-center">
                                <button
                                  className="btn btn-success"
                                  type="submit"
                                >
                                  Add picture
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </Form>
                    )}
                  </Formik>
                  <div className="row">
                    {galleries.map((item, key) => {
                      return (
                        <div key={key} className="col-md-4">
                          <Card className="bg-dark text-white">
                            <Card.Img
                              src={item.imageSrc}
                              alt="Card image"
                              onError={onImageError}
                            />
                            <Card.ImgOverlay>
                              <Card.Title>
                                <div className="text-right">
                                  <button
                                    className="btn btn-danger btn-sm"
                                    onClick={() => confirmDelete(item.id)}
                                  >
                                    <i className="fas fa-times fs-4"></i>
                                  </button>
                                </div>
                              </Card.Title>
                            </Card.ImgOverlay>
                          </Card>
                        </div>
                      );
                    })}
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
      {/* row */}
    </div>
  );
};

export default AddProperty;
