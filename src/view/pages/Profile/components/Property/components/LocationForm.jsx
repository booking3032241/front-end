import { useEffect } from "react";

const LocationForm = (props) => {
  useEffect(() => {
    props.setFieldValue("address", props.address);
    props.setFieldValue("city", props.city);
    props.setFieldValue("country", props.country);
    props.setFieldValue("area", props.area);
    props.setFieldValue("state", props.state);
    props.setFieldValue("latitude", props.latitude);
    props.setFieldValue("longitude", props.longitude);
  }, [props.address]);

  return (
    <div className="row">
      <div className="form-group col-md-6">
        <label>Address</label>
        <input
          name="address"
          type="text"
          defaultValue={props.values["address"]}
          className={
            "form-control  " +
            (props.errors.address && props.touched.address
              ? " is-invalid"
              : "") +
            (!props.errors.address && props.touched.address ? " is-valid" : "")
          }
          onChange={(e) => props.handleChange(e)}
        />
      </div>
      {props.errors.address && (
        <div className="invalid-feedback d-flex mb-3">
          {props.errors.address}
        </div>
      )}
      <div className="form-group col-md-6">
        <label>country</label>
        <input
          name="country"
          type="text"
          defaultValue={props.values["country"]}
          className={
            "form-control  " +
            (props.errors.country && props.touched.country
              ? " is-invalid"
              : "") +
            (!props.errors.country && props.touched.country ? " is-valid" : "")
          }
          onChange={(e) => props.handleChange(e)}
        />
      </div>
      {props.errors.country && (
        <div className="invalid-feedback d-flex mb-3">
          {props.errors.country}
        </div>
      )}
      <div className="form-group col-md-6">
        <label>city</label>
        <input
          name="city"
          type="text"
          defaultValue={props.values["city"]}
          className={
            "form-control  " +
            (props.errors.city && props.touched.city ? " is-invalid" : "") +
            (!props.errors.city && props.touched.city ? " is-valid" : "")
          }
          onChange={(e) => props.handleChange(e)}
        />
      </div>
      {props.errors.city && (
        <div className="invalid-feedback d-flex mb-3">{props.errors.city}</div>
      )}
      <div className="form-group col-md-6">
        <label>state</label>
        <input
          name="state"
          type="text"
          defaultValue={props.values["state"]}
          className={
            "form-control  " +
            (props.errors.state && props.touched.state ? " is-invalid" : "") +
            (!props.errors.state && props.touched.state ? " is-valid" : "")
          }
          onChange={(e) => props.handleChange(e)}
        />
      </div>
      {props.errors.state && (
        <div className="invalid-feedback d-flex mb-3">{props.errors.state}</div>
      )}
    </div>
  );
};

export default LocationForm;
