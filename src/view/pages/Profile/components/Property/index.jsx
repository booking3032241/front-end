import { useEffect, useState } from "react";
import { renderNature } from "helpers/GeneralFunctions";
import api from "api";
import { useNavigate } from "react-router-dom";
import _ from "lodash";
import Empty from "components/Empty";

const Index = ({ ownerId }) => {
  const navigate = useNavigate();
  const [properties, setProperties] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const fetchProperties = () => {
    api
      .get(`estates/search?ownerId=${ownerId}`)
      .then((response) => {
        setProperties(response.data);
        setIsLoading(false);
      })
      .catch((error) => {
        var message = error.response.data;
        setIsLoading(false);
      });
  };
  useEffect(() => {
    fetchProperties();
  }, []);
  return (
    <div className="dashboard-body">
      <div className="row">
        <div className="col-lg-12 col-md-12">
          <div className="_prt_filt_dash">
            <div className="_prt_filt_dash_flex">
              <div
                className="btn-group"
                role="group"
                aria-label="Basic radio toggle button group"
              >
                <input
                  type="radio"
                  className="btn-check"
                  name="chooselists"
                  id="alllist"
                  defaultChecked=""
                />
                <label
                  className="btn btn-md btn-outline-primary font--medium"
                  htmlFor="alllist"
                >
                  All
                </label>
                <input
                  type="radio"
                  className="btn-check"
                  name="chooselists"
                  id="activelist"
                />
                <label
                  className="btn btn-md btn-outline-primary font--medium"
                  htmlFor="activelist"
                >
                  Active
                </label>
                <input
                  type="radio"
                  className="btn-check"
                  name="chooselists"
                  id="expiredlist"
                />
                <label
                  className="btn btn-md btn-outline-primary font--medium"
                  htmlFor="expiredlist"
                >
                  Expired
                </label>
                <input
                  type="radio"
                  className="btn-check"
                  name="chooselists"
                  id="draftlist"
                />
                <label
                  className="btn btn-md btn-outline-primary font--medium"
                  htmlFor="draftlist"
                >
                  Draft
                </label>
              </div>
            </div>
            <div className="_prt_filt_dash_last m2_hide">
              <div className="_prt_filt_radius"></div>
              <div className="_prt_filt_add_new">
                <a
                  onClick={() => navigate("/profile/add-property")}
                  className="prt_submit_link cursor-pointer"
                >
                  <i className="fas fa-plus-circle" />
                  <span className="d-none d-lg-block d-md-block">
                    New Property
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12 col-md-12">
          <div className="dashboard_property">
            {!_.isEmpty(properties) ? (
              <div className="table-responsive">
                <table className="table">
                  <thead className="thead-dark">
                    <tr>
                      <th scope="col">Property</th>

                      <th scope="col" className="m2_hide">
                        Nature
                      </th>

                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {properties.map((item, key) => {
                      return (
                        <tr key={key}>
                          <td>
                            <div className="dash_prt_wrap">
                              <div className="dash_prt_thumb">
                                <img
                                  src={item.defaultImageSrc}
                                  className="img-fluid"
                                  style={{ width: "100%", maxWidth: "100px" }}
                                  alt=""
                                />
                              </div>
                              <div className="dash_prt_caption">
                                <h5>{item.name}</h5>
                                <div className="prt_dashb_lot">
                                  {item.address}
                                </div>
                                <div className="prt_dash_rate">
                                  <span>{item.price}</span>
                                </div>
                              </div>
                            </div>
                          </td>

                          <td className="m2_hide">
                            {renderNature(item.nature)}
                          </td>

                          <td>
                            <div className="_leads_action">
                              <a
                                className="cursor-pointer"
                                onClick={() =>
                                  navigate("/profile/edit-property/" + item.id)
                                }
                              >
                                <i className="fas fa-edit" />
                              </a>
                              <a
                                className="cursor-pointer"
                                onClick={() =>
                                  navigate("/profile/my-property/" + item.id)
                                }
                              >
                                <i className="fas fa-eye"></i>
                              </a>
                              <a href="#">
                                <i className="fas fa-trash" />
                              </a>
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            ) : (
              <div className="d-flex justify-content-center mt-3">
                <Empty
                  description={"No properties."}
                  img_heigth="120px"
                  img_width="120px"
                />
              </div>
            )}
          </div>
        </div>
      </div>
      {/* row */}
    </div>
  );
};

export default Index;
