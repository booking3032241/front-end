
const index = () => {
  return (
    <div><div className="dashboard-body">
    <div className="dashboard-wraper">
      {/* Basic Information */}
      <div className="frm_submit_block">
        <h4>Change Your Password</h4>
        <div className="frm_submit_wrap">
          <div className="row">
            <div className="form-group col-lg-12 col-md-6">
              <label>Old Password</label>
              <input type="password" className="form-control" />
            </div>
            <div className="form-group col-md-6">
              <label>New Password</label>
              <input type="password" className="form-control" />
            </div>
            <div className="form-group col-md-6">
              <label>Confirm password</label>
              <input type="password" className="form-control" />
            </div>
            <div className="form-group col-lg-12 col-md-12">
              <button className="btn btn-primary" type="submit">
                Save Changes
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  )
}

export default index