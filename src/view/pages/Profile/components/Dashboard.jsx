const Dashboard = () => {
  return (
    <div className="dashboard-body">
      <div className="row">
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="veshm-dash-wrapper widget-1">
            <div className="veshm-dash-icon">
              <div className="vsm-icon-lio bg-light-success text-success">
                <i className="fa-solid fa-sack-dollar" />
              </div>
            </div>
            <div className="veshm-dash-block">
              <div className="veshm-dash-block_content">
                <h4>
                  $872M
                  <span className="label font--medium label-purple me-2">
                    <i className="fa-solid fa-arrow-trend-down" />
                    07%
                  </span>
                </h4>
                <h6>Total Income</h6>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="veshm-dash-wrapper widget-2">
            <div className="veshm-dash-icon">
              <div className="vsm-icon-lio bg-light-danger text-danger">
                <i className="fa-brands fa-gg-circle" />
              </div>
            </div>
            <div className="veshm-dash-block">
              <div className="veshm-dash-block_content">
                <h4>2415</h4>
                <h6>Total Property</h6>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6 col-sm-12">
          <div className="veshm-dash-wrapper widget-3">
            <div className="veshm-dash-icon">
              <div className="vsm-icon-lio bg-light-warning text-warning">
                <i className="fa-solid fa-chart-line" />
              </div>
            </div>
            <div className="veshm-dash-block">
              <div className="veshm-dash-block_content">
                <h4>
                  $.68M
                  <span className="label font--medium label-success me-2">
                    <i className="fa-solid fa-arrow-trend-up" />
                    12%
                  </span>
                </h4>
                <h6>Total Revenue</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
