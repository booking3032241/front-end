import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useAuth } from "components/AuthContext";

const Menu = () => {
  const auth = useAuth();
  const navigate = useNavigate();
  const location = useLocation();
  const pathname = location.pathname;

  const renderMenuIdUsingLocation = (pathname) => {
    if (pathname.includes("my-profile")) {
      return 2;
    } else if (
      pathname.includes("my-properties") ||
      pathname.includes("edit-property") ||
      pathname.includes("my-property")
    ) {
      return 3;
    } else if (pathname.includes("add-property")) {
      return 4;
    } else if (pathname.includes("my-account")) {
      return 5;
    }

    return 1;
  };

  const [menuId, setMenuId] = useState(renderMenuIdUsingLocation(pathname));

  const menuList = [
    {
      id: 1,
      name: "Dashboard",
      link: "/profile/dashboard",
      iconClass: "fa fa-tachometer-alt",
    },
    {
      id: 2,
      name: "My profile",
      link: "/profile/my-profile",
      iconClass: "fa fa-user-tie",
    },
    {
      id: 3,
      name: "My properties",
      link: "/profile/my-properties",
      iconClass: "fa fa-pen-nib",
    },
    {
      id: 4,
      name: "Add new properties",
      link: "/profile/add-property",
      iconClass: "fa fa-unlock-alt",
    },
    {
      id: 5,
      name: "My account",
      link: "/profile/my-account",
      iconClass: "fa fa-user-tie",
    },
  ];
  function renderMenuId() {
    const menuId = renderMenuIdUsingLocation(pathname);
    setMenuId(menuId);
  }
  useEffect(() => {
    renderMenuId();
  }, [pathname]);
  return (
    <>
      <div className="dash_user_menues">
        <ul>
          {menuList.map((item, key) => (
            <li
              key={key}
              className={
                "cursor-pointer " + (item.id == menuId ? "active" : "")
              }
            >
              <a
                onClick={() => {
                  navigate(item.link);
                  setMenuId(item.id);
                }}
              >
                <i className={item.iconClass} />
                {item.name}
              </a>
            </li>
          ))}
        </ul>
      </div>
      <div className="dash_user_footer">
        <ul
          style={{
            justifyContent: "center",
            background: "#fa014f",
            color: "white",
          }}
          className="cursor-pointer"
          onClick={() => auth.logout()}
        >
          <li>
            <a>
              <i className="fa fa-power-off me-2" />
              Logout
            </a>
          </li>
        </ul>
      </div>
    </>
  );
};

export default Menu;
