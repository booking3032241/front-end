import { Route, Routes } from "react-router-dom";
import Dashboard from "./components/Dashboard";
import Profile from "./components/Profile/index";
import Account from "./components/Account";
import Properties from "./components/Property/index.jsx";
import Property from "./components/Property/components/ViewProperty";
import AddProperty from "./components/Property/components/AddProperty";
import EditProperty from "./components/Property/components/EditProperty";
import { useEffect, useState } from "react";
import { useAuth } from "components/AuthContext";
import api from "api";
import EditProfile from "./components/Profile/components/EditProfile";
import GeneralInfos from "./components/GeneralInfos/index";
import Menu from "./components/Menu";
import Loading from "components/Loading";
import Empty from "components/Empty";
import _ from "lodash";
const Index = () => {
  const auth = useAuth();
  const user = auth.user;
  const userId = user ? user.id : "";
  const [isLoading, setIsLoading] = useState(true);
  const [owner, setOwner] = useState([]);

  const fetchOwner = () => {
    if (userId) {
      api
        .get(`owners/user/${userId}`)
        .then((response) => {
          setOwner(response.data);
          setIsLoading(false);
        })
        .catch((error) => {
          console.log(error);
          setIsLoading(false);
        });
    }
  };

  useEffect(() => {
    fetchOwner();
  }, []);
  return (
    <>
      {/* ============================ Page Title Start================================== */}
      <div
        className="page-title"
        style={{
          background: `#017efa url(/src/assets/media//page-title.png) no-repeat`,
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12">
              <h2 className="ipt-title">Hi, {user?.username}</h2>
              <span className="ipn-subtitle">
                Manage your profile and view property
              </span>
            </div>
          </div>
        </div>
      </div>
      {/* ============================ Page Title End ================================== */}
      {/* ============================ User Dashboard ================================== */}
      <Loading isLoading={isLoading}>
        <section className="gray-simple pt-5 pb-5">
          <div className="container-fluid">
            {!_.isEmpty(owner) ? (
              <div className="row">
                <div className="col-xl-3 col-lg-4 col-md-12">
                  <div className="property_dashboard_navbar">
                    <GeneralInfos owner={owner} fetchOwner={fetchOwner} />
                    <Menu />
                  </div>
                </div>
                <div className="col-xl-9 col-lg-8 col-md-12">
                  <Routes>
                    <Route path={"dashboard"} element={<Dashboard />} />
                    <Route
                      path={"my-account"}
                      element={<Account fetchOwner={fetchOwner} />}
                    />
                    <Route
                      path={"my-profile"}
                      element={<Profile owner={owner} />}
                    />
                    <Route
                      path={"my-profile/edit"}
                      element={
                        <EditProfile owner={owner} fetchOwner={fetchOwner} />
                      }
                    />
                    <Route
                      path={"my-properties"}
                      element={<Properties ownerId={owner.id} />}
                    />
                    <Route
                      path={"my-property/:propertyId"}
                      element={<Property />}
                    />
                    <Route
                      path={"add-property"}
                      element={<AddProperty ownerId={owner.id} />}
                    />
                    <Route
                      path={"edit-property/:propertyId"}
                      element={<EditProperty />}
                    />
                  </Routes>
                </div>
              </div>
            ) : (
              <div className="d-flex justify-content-center">
                <Empty
                  description={"No profile."}
                  img_heigth="120px"
                  img_width="120px"
                />
              </div>
            )}
          </div>
        </section>
      </Loading>
    </>
  );
};

export default Index;
