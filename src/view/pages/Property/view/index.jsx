import { useParams } from "react-router-dom";
import api from "api";
import { useEffect, useState } from "react";
import _ from "lodash";
import Loading from "components/Loading";
import CustomSlider from "components/CustomSlider";
import ProfileImage from "assets/media/profile-image.jpg";
import EmptyImage from "assets/media/EmptyImage.png";
import MapPosition from "../../../../components/MapPosition";

const Index = () => {
  const params = useParams();
  const propertyId = params.propertyId;
  const [property, setProperty] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const fetchProperty = () => {
    api
      .get(`estates/${propertyId}`)
      .then((response) => {
        setProperty(response.data);
        setIsLoading(false);
      })
      .catch((error) => {
        var message = error.response.data;
        setIsLoading(false);
      });
  };
  const onImageError = (e) => {
    e.target.src = ProfileImage;
  };
  const onSliderImageError = (e) => {
    e.target.src = EmptyImage;
  };
  useEffect(() => {
    fetchProperty();
  }, []);
  return (
    <>
      {/* ============================ Hero Banner  Start================================== */}
      <div className="featured_slick_gallery gray">
        <div className="featured_slick_gallery-slide">
          <Loading isLoading={isLoading}>
            {!_.isEmpty(property?.galleries) && (
              <CustomSlider data={property.galleries}>
                {property.galleries.map((item, key) => {
                  return (
                    <div key={key} className="featured_slick_padd mfp-gallery">
                      <img
                        src={item.imageSrc}
                        className="img-fluid mx-auto"
                        style={{ height: "496px", width: "100%" }}
                        alt=""
                        onError={onSliderImageError}
                      />
                    </div>
                  );
                })}
              </CustomSlider>
            )}
          </Loading>
        </div>
      </div>
      {/* ============================ Hero Banner End ================================== */}
      {/* ============================ Property Detail Start ================================== */}
      <section className="gray-simple">
        <div className="container">
          <div className="row">
            {/* property main detail */}
            <div className="col-lg-8 col-md-12 col-sm-12">
              {/* Main Info Detail */}
              <div className="vesh-detail-bloc">
                <div className="vesh-detail-headup">
                  <div className="vesh-detail-headup-first">
                    <div className="prt-detail-title-desc">
                      <span className="label label-success">For Sale</span>
                      <h4>{property.name}</h4>
                      <span className="text-mid">
                        <i className="fa-solid fa-location-dot me-2" />
                        {property.address}
                      </span>
                      <div className="list-fx-features mt-2">
                        <div className="list-fx-fisrt">
                          <span className="label font--medium label-light-success me-2">
                            {property.bedrooms} Beds
                          </span>
                          <span className="label font--medium label-light-info me-2">
                            {property.bathrooms} Bath
                          </span>
                          <span className=" label font--medium label-light-danger">
                            {property.space} Sqft
                          </span>
                        </div>
                        <div className="list-fx-last"></div>
                      </div>
                    </div>
                  </div>
                  <div className="vesh-detail-headup-last">
                    <h3 className="prt-price-fix theme-cl">
                      ${property.price}
                      <span>One Time</span>
                    </h3>
                  </div>
                </div>
              </div>
              {/* About Property Detail */}
              <div className="vesh-detail-bloc">
                <div className="vesh-detail-bloc_header">
                  <h4 className="property_block_title no-arrow">
                    About Property
                  </h4>
                </div>
                <div className="vesh-detail-bloc-body">
                  <div className="row g-3">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                      <p>{property.description}</p>
                    </div>
                  </div>
                </div>
              </div>
              {/* Basic Detail */}
              <div className="vesh-detail-bloc">
                <div className="vesh-detail-bloc_header">
                  <a
                    data-bs-toggle="collapse"
                    data-parent="#basicinfo"
                    data-bs-target="#basicinfo"
                    aria-controls="basicinfo"
                    aria-expanded="false"
                  >
                    <h4 className="property_block_title">Basic Detail</h4>
                  </a>
                </div>
                <div
                  id="basicinfo"
                  className="panel-collapse collapse show"
                  aria-labelledby="basicinfo"
                >
                  <div className="vesh-detail-bloc-body">
                    <div className="row g-3">
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-solid fa-bed" />
                          </div>
                          <div className="ilio-text">3 Bedrooms</div>
                        </div>
                      </div>
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-solid fa-bath" />
                          </div>
                          <div className="ilio-text">2 Bathrooms</div>
                        </div>
                      </div>
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-solid fa-layer-group" />
                          </div>
                          <div className="ilio-text">4,240 sq ft</div>
                        </div>
                      </div>
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-solid fa-warehouse" />
                          </div>
                          <div className="ilio-text">1 Garage</div>
                        </div>
                      </div>
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-regular fa-building" />
                          </div>
                          <div className="ilio-text">Apartment</div>
                        </div>
                      </div>
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-solid fa-building-wheat" />
                          </div>
                          <div className="ilio-text">Built 1982</div>
                        </div>
                      </div>
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-solid fa-building-circle-check" />
                          </div>
                          <div className="ilio-text">Active</div>
                        </div>
                      </div>
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-solid fa-fan" />
                          </div>
                          <div className="ilio-text">Central A/C</div>
                        </div>
                      </div>
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-regular fa-snowflake" />
                          </div>
                          <div className="ilio-text">Forced Air</div>
                        </div>
                      </div>
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-solid fa-bowl-food" />
                          </div>
                          <div className="ilio-text">Kitchen Facilities</div>
                        </div>
                      </div>
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-solid fa-martini-glass-citrus" />
                          </div>
                          <div className="ilio-text">Bar &amp; Drinks</div>
                        </div>
                      </div>
                      <div className="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6">
                        <div className="ilio-icon-wrap">
                          <div className="ilio-icon">
                            <i className="fa-regular fa-building" />
                          </div>
                          <div className="ilio-text">4 Floor</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="vesh-detail-bloc">
                <div className="vesh-detail-bloc_header">
                  <h4 className="property_block_title no-arrow">Location</h4>
                </div>
                <div className="vesh-detail-bloc-body">
                  <MapPosition
                    lat={property.latitude}
                    lng={property.longitude}
                  />
                </div>
              </div>
            </div>
            {/* property main detail */}
            {/* Property Sidebar */}
            <div className="col-lg-4 col-md-12 col-sm-12">
              {/* Like And Share */}
              <div className="vesh-detail-bloc">
                <ul className="like_share_list">
                  <li>
                    <a
                      className="btn btn-light-success"
                      data-toggle="tooltip"
                      data-original-title="Share"
                    >
                      <i className="fa-solid fa-share-from-square me-2" />
                      Share
                    </a>
                  </li>
                  <li>
                    <a
                      className="btn btn-light-danger"
                      data-toggle="tooltip"
                      data-original-title="Save"
                    >
                      <i className="fa-solid fa-heart-circle-check me-2" />
                      Save
                    </a>
                  </li>
                  <li>
                    <a
                      className="btn btn-light-primary"
                      data-toggle="tooltip"
                      data-original-title="Save"
                    >
                      <i className="fa-solid fa-print me-2" />
                      Print
                    </a>
                  </li>
                </ul>
              </div>
              <div className="pg-side-groups">
                <div className="pg-side-block">
                  <div className="pg-side-block-head">
                    <h5 className="mb-0">Book your Room</h5>
                  </div>
                  <div className="pg-side-block-body">
                    <div className="pg-side-block-info pt-0 pb-4">
                      <div className="sides-widget-body simple-form mt-2">
                        <div className="form-row row">
                          <div className="form-group col-6">
                            <label className="font--bold">Check In</label>
                            <input
                              type="text"
                              id="checkin"
                              data-dd-opt-custom-class="datedrp"
                              data-dd-opt-expanded-default="true"
                              className="form-control"
                              placeholder="Check IN"
                            />
                          </div>
                          <div className="form-group col-6">
                            <label className="font--bold">Check Out</label>
                            <input
                              type="text"
                              id="checkout"
                              data-dd-opt-custom-class="datedrp"
                              data-dd-opt-expanded-default="true"
                              className="form-control"
                              placeholder="Check Out"
                            />
                          </div>
                          <div className="form-group col-6">
                            <label>
                              Adults
                              <span className="label label-success ms-1 px-2">
                                $299/Per Day
                              </span>
                            </label>
                            <select className="form-control">
                              <option value={1}>01 Adults</option>
                              <option value={2}>02 Adults</option>
                              <option value={3}>03 Adults</option>
                              <option value={4}>04 Adults</option>
                              <option value={5}>05 Adults</option>
                              <option value={6}>06 Adults</option>
                              <option value={7}>07 Adults</option>
                              <option value={8}>08 Adults</option>
                              <option value={9}>09 Adults</option>
                              <option value={10}>10 Adults</option>
                            </select>
                          </div>
                          <div className="form-group col-6">
                            <label>Children</label>
                            <select className="form-control">
                              <option value={1}>0 Adults</option>
                              <option value={2}>01 Adults</option>
                              <option value={3}>02 Adults</option>
                              <option value={4}>03 Adults</option>
                              <option value={5}>04 Adults</option>
                              <option value={6}>05 Adults</option>
                              <option value={7}>06 Adults</option>
                              <option value={8}>07 Adults</option>
                              <option value={9}>08 Adults</option>
                              <option value={10}>9 Adults</option>
                              <option value={11}>10 Adults</option>
                            </select>
                          </div>
                        </div>
                        <div className="form-group row">
                          <div className="col-12">
                            <h6>Add Custom Amenties</h6>
                          </div>
                          <div className="col-12">
                            <input
                              type="checkbox"
                              className="btn-check"
                              id="Conditioning"
                            />
                            <label
                              className="btn btn-md btn-outline-primary"
                              htmlFor="Conditioning"
                            >
                              Air Conditioning
                            </label>
                            <input
                              type="checkbox"
                              className="btn-check"
                              id="Internet"
                            />
                            <label
                              className="btn btn-md btn-outline-primary"
                              htmlFor="Internet"
                            >
                              Internet
                            </label>
                            <input
                              type="checkbox"
                              className="btn-check"
                              id="massage"
                              defaultChecked=""
                            />
                            <label
                              className="btn btn-md btn-outline-primary"
                              htmlFor="massage"
                            >
                              Spa &amp; Massage
                            </label>
                            <input
                              type="checkbox"
                              className="btn-check"
                              id="Laundry"
                            />
                            <label
                              className="btn btn-md btn-outline-primary"
                              htmlFor="Laundry"
                            >
                              Laundry
                            </label>
                            <input
                              type="checkbox"
                              className="btn-check"
                              id="Accessible"
                              defaultChecked=""
                            />
                            <label
                              className="btn btn-md btn-outline-primary"
                              htmlFor="Accessible"
                            >
                              Chair Accessible
                            </label>
                            <input
                              type="checkbox"
                              className="btn-check"
                              id="yard"
                            />
                            <label
                              className="btn btn-md btn-outline-primary"
                              htmlFor="yard"
                            >
                              Front yard
                            </label>
                            <input
                              type="checkbox"
                              className="btn-check"
                              id="Smoking"
                              defaultChecked=""
                            />
                            <label
                              className="btn btn-md btn-outline-primary"
                              htmlFor="Smoking"
                            >
                              Smoking
                            </label>
                            <input
                              type="checkbox"
                              className="btn-check"
                              id="parking"
                            />
                            <label
                              className="btn btn-md btn-outline-primary"
                              htmlFor="parking"
                            >
                              Car Parking
                            </label>
                            <input
                              type="checkbox"
                              className="btn-check"
                              id="Food"
                            />
                            <label
                              className="btn btn-md btn-outline-primary"
                              htmlFor="Food"
                            >
                              Fast Food
                            </label>
                          </div>
                        </div>
                      </div>
                      <div className="sides-widget-body simple-form">
                        <div className="form-group">
                          <label className="font--bold">Name*</label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Your Name"
                          />
                        </div>
                        <div className="form-group">
                          <label>Email*</label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Your Email"
                          />
                        </div>
                        <div className="form-group">
                          <label>Phone No.*</label>
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Your Phone"
                          />
                        </div>
                        <div className="form-group">
                          <label>Description</label>
                          <textarea
                            className="form-control"
                            defaultValue={"I'm interested in this property."}
                          />
                        </div>
                        <div className="form-group">
                          <div className="booking-info-try rounded bg-light-success border border-success">
                            <ul>
                              <li>
                                <h6>Adults</h6>
                                <span>$299x4 = $1196</span>
                              </li>
                              <li>
                                <h6>Children</h6>
                                <span>$0x4 = $00</span>
                              </li>
                              <li>
                                <h6>Spa &amp; Massage</h6>
                                <span>$29x4 = $116</span>
                              </li>
                              <li>
                                <h6>Chair Accessible</h6>
                                <span>$50X4 = $200</span>
                              </li>
                              <li>
                                <h6>Smoking</h6>
                                <span>$15x4 = $60</span>
                              </li>
                              <li>
                                <h6 className="total-title">Total</h6>
                                <span className="total-price">$1760</span>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <button className="btn btn-theme font--medium full-width">
                          Submit &amp; Book
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="pg-side-block">
                  <div className="pg-side-block-head">
                    <div className="pg-side-left">
                      <div className="pg-side-thumb">
                        {property.owner?.defaultImageSrc ? (
                          <img
                            src={property.owner.defaultImageSrc}
                            className="img-fluid circle"
                            alt=""
                            onError={onImageError}
                          />
                        ) : (
                          <img
                            src={ProfileImage}
                            className="img-fluid circle"
                            alt=""
                            onError={onImageError}
                          />
                        )}
                      </div>
                    </div>
                    <div className="pg-side-right">
                      <div className="pg-side-right-caption">
                        <h4>{`${property.owner?.firstname} ${property.owner?.lastname}`}</h4>
                        <span>
                          <i className="fa-solid fa-location-dot me-2" />
                          {property.owner?.address}
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="pg-side-block-body">
                    <div className="pg-side-block-info">
                      <div className="vl-elfo-group">
                        <div className="vl-elfo-icon">
                          <i className="fa-solid fa-phone-volume" />
                        </div>
                        <div className="vl-elfo-caption">
                          <h6>Call Us</h6>
                          <p>{property.owner?.phone}</p>
                        </div>
                      </div>
                      <div className="vl-elfo-group">
                        <div className="vl-elfo-icon">
                          <i className="fa-regular fa-envelope" />
                        </div>
                        <div className="vl-elfo-caption">
                          <h6>Drop A Mail</h6>
                          <p>{property.owner?.email}</p>
                        </div>
                      </div>
                    </div>
                    <div className="pg-side-block-buttons">
                      <div className="single-button">
                        <a
                          data-bs-toggle="modal"
                          data-bs-target="#offer"
                          className="btn font--medium btn-light-success full-width"
                        >
                          <i className="fa-solid fa-paper-plane me-2" />
                          Send An offer
                        </a>
                      </div>
                      <div className="single-button">
                        <a
                          data-bs-toggle="modal"
                          data-bs-target="#message"
                          className="btn font--medium btn-theme full-width"
                        >
                          <i className="fa-solid fa-comments me-2" />
                          Send A Message
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* ============================ Property Detail End ================================== */}
    </>
  );
};

export default Index;
