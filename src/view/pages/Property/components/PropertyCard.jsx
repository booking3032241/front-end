import _ from "lodash";
import { useNavigate } from "react-router-dom";
import Slider from "react-slick";
import EmptyImage from "assets/media/EmptyImage.png";

const PropertyCard = ({ property }) => {
  const navigate = useNavigate();
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    <div className="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
      <div className="veshm-list-prty">
        <div className="veshm-list-prty-figure">
          <div className="veshm-list-img-slide">
            <div className="veshm-list-click">
              <Slider {...settings}>
                {!_.isEmpty(property.galleries) ? (
                  property.galleries.map((elt, k2) => {
                    return (
                      <a
                        key={k2}
                        onClick={() => navigate(`/properties/${property.id}`)}
                        className="cursor-pointer"
                      >
                        <img
                          src={elt.imageSrc}
                          className="img-fluid mx-auto"
                          alt=""
                        />
                      </a>
                    );
                  })
                ) : (
                  <img
                    onClick={() => navigate(`/properties/${property.id}`)}
                    className="cursor-pointer"
                    src={EmptyImage}
                  />
                )}
              </Slider>
            </div>
          </div>
        </div>
        <div className="veshm-list-prty-caption">
          <div className="veshm-list-kygf">
            <div className="veshm-list-kygf-flex">
              {property.nature == 1 ? (
                <div className="veshm-list-typess rent">
                  <span>For Rent</span>
                </div>
              ) : (
                <div className="veshm-list-typess sale">
                  <span>For Sale</span>
                </div>
              )}
              <div className="rlhc-price mb-2">
                <h4 className="rlhc-price-name theme-cl">
                  $ {property.price}
                  <span className="monthly">/Months</span>
                </h4>
              </div>
              <h5 className="rlhc-title-name verified">
                <a
                  onClick={() => navigate(`/properties/${property.id}`)}
                  className="cursor-pointer prt-link-detail"
                >
                  {property.name}
                </a>
              </h5>
              <div className="vesh-aget-rates">
                <i className="fa-solid fa-star" />
                <i className="fa-solid fa-star" />
                <i className="fa-solid fa-star" />
                <i className="fa-solid fa-star" />
                <i className="fa-solid fa-star" />
                <span className="resy-98">340 Reviews</span>
              </div>
            </div>
            <div className="veshm-list-head-flex">
              <button className="btn btn-like active" type="button">
                <i className="fa-solid fa-heart-circle-check" />
              </button>
            </div>
          </div>
          <div className="veshm-list-middle">
            <p className="text-mid">{property.address}</p>
          </div>
          <div className="veshm-list-footers">
            <div className="veshm-list-icons">
              <ul>
                {property.bedrooms && (
                  <li>
                    <i className="fa-solid fa-bed" />
                    <span> {property.bedrooms} Bed</span>
                  </li>
                )}
                {property.bathrooms && (
                  <li>
                    <i className="fa-solid fa-bath" />
                    <span> {property.bathrooms} Ba</span>
                  </li>
                )}
                {property.space && (
                  <li>
                    <i className="fa-solid fa-vector-square" />
                    <span> {property.space} Sqft</span>
                  </li>
                )}
                <li>
                  <i className="fa-solid fa-calendar-days" />
                  <span>Built 2017</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PropertyCard;
