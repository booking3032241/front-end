import { lazy, useEffect, useState } from "react";
import api from "api";
import _ from "lodash";
import Empty from "components/Empty";
import Loading from "components/Loading";
const PropertyCard = lazy(() => import("./components/PropertyCard"));

const Index = () => {
  const [properties, setProperties] = useState([]);
  const [searchName, setSearchName] = useState("");
  const [searchPtype, setSearchPtype] = useState(0);
  const [searchCity, setSearchCity] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  const fetchProperties = (name = "", ptype = 0, city = "") => {
    api
      .get(`estates/search?name=${name}&ptype=${ptype}&city=${city}`)
      .then((response) => {
        setProperties(response.data);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
      });
  };
  const setSearchText = (event, searchField) => {
    var searchText = event.target.value;
    switch (searchField) {
      case "name":
        setSearchName(searchText);
        break;
      case "ptype":
        setSearchPtype(searchText);
        break;
      case "city":
        setSearchCity(searchText);
        break;

      default:
        break;
    }
  };
  const launchSearch = () => {
    fetchProperties(searchName, searchPtype, searchCity);
  };
  useEffect(() => {
    fetchProperties();
  }, []);
  return (
    <>
      {/* ============================ Page Title Start================================== */}
      <div
        className="page-title"
        style={{
          background:
            "#017efa url(/src/assets/media//page-title.png) no-repeat",
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-md-12"></div>
          </div>
        </div>
      </div>
      {/* ============================ Page Title End ================================== */}
      {/* ============================ All Property ================================== */}
      <section className="over-top micler gray-simple">
        <div className="container">
          {/* Start Search */}
          <div className="row">
            <div className="col-xl-12 col-lg-12 col-md-12">
              <div className="full-search-2 mt-2">
                <div className="hero-search-content colored">
                  <div className="row classic-search-box m-0 gx-2">
                    <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12">
                      <div className="form-group briod">
                        <div className="input-with-icon">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="Ex. villa, town etc."
                            onChange={(e) => setSearchText(e, "name")}
                          />
                          <i className="fa-solid fa-magnifying-glass" />
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12">
                      <div className="form-group briod">
                        <div className="input-with-icon">
                          <select
                            className="form-control"
                            onChange={(e) => setSearchText(e, "ptype")}
                          >
                            <option>Property types</option>
                            <option value={1}>Houses</option>
                            <option value={2}>Apartment</option>
                            <option value={3}>Villas</option>
                            <option value={4}>Commercial</option>
                            <option value={5}>Offices</option>
                            <option value={6}>Garage</option>
                          </select>
                          <i className="fa-solid fa-house-crack" />
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12">
                      <div className="form-group">
                        <div className="input-with-icon">
                          <input
                            type="text"
                            className="form-control"
                            placeholder="City ..."
                            onChange={(e) => setSearchText(e, "city")}
                          />
                          <i className="fa-solid fa-location-crosshairs" />
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-3 col-lg-3 col-md-12 col-sm-12">
                      <div className="fliox-search-wiop">
                        <div className="form-group">
                          <button
                            type="submit"
                            className="btn btn-primary full-width"
                            onClick={() => launchSearch()}
                          >
                            Search
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Loading isLoading={isLoading}>
            {/* Start All Listing */}
            <div className="row gx-3 gy-4 mt-4">
              {!_.isEmpty(properties) ? (
                properties.map((item, key) => {
                  return <PropertyCard key={key} property={item} />;
                })
              ) : (
                <div className="d-flex justify-content-center">
                  <Empty
                    description={"No properties."}
                    img_heigth="120px"
                    img_width="120px"
                  />
                </div>
              )}
            </div>
            {/* Start Pagination */}
            <div className="row">
              <div className="col-lg-12 col-md-12 col-sm-12">
                <nav aria-label="Page navigation example">
                  <ul className="pagination">
                    <li className="page-item">
                      <a
                        className="page-link"
                        href="JavaScript:Void(0);"
                        aria-label="Previous"
                      >
                        <span aria-hidden="true">«</span>
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="JavaScript:Void(0);">
                        1
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="JavaScript:Void(0);">
                        2
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="JavaScript:Void(0);">
                        3
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="JavaScript:Void(0);">
                        4
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="JavaScript:Void(0);">
                        5
                      </a>
                    </li>
                    <li className="page-item">
                      <a className="page-link" href="JavaScript:Void(0);">
                        6
                      </a>
                    </li>
                    <li className="page-item">
                      <a
                        className="page-link"
                        href="JavaScript:Void(0);"
                        aria-label="Next"
                      >
                        <span aria-hidden="true">»</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </Loading>
        </div>
      </section>
      {/* ============================ All Property ================================== */}
    </>
  );
};

export default Index;
