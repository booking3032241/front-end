import { Route, Routes } from "react-router-dom";
import Home from "pages/Home";
import Profile from "pages/Profile";
import Properties from "pages/Property";
import Property from "pages/Property/view";
import { RequireAuth } from "./RequireAuth";
import PageNotFound from "pages/CustomPages/PageNotFound";
import NetworkError from "pages/CustomPages/NetworkError";
// import Map from "pages/Map";
const Content = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      {/* <Route path="/map" element={<Map />} /> */}
      <Route path="/network-error" element={<NetworkError />} />
      <Route path="/properties" element={<Properties />} />
      <Route path="/properties/:propertyId" element={<Property />} />
      <Route
        path="/profile/*"
        element={
          <RequireAuth>
            <Profile />
          </RequireAuth>
        }
      />
      <Route path="*" element={<PageNotFound />} />
    </Routes>
  );
};

export default Content;
