import { useNavigate } from "react-router-dom";
import Logo from "assets/media/booking-logo.png";

const Footer = () => {
  const navigate = useNavigate();

  return (
    <footer className="skin-light-footer shadow-lg">
      <div>
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-4">
              <div className="footer-widget">
                <img
                  src={Logo}
                  className="img-footer"
                  alt=""
                  onClick={() => navigate("/")}
                />
                <div className="footer-add">
                  <p>Collins Street West, Victoria 8007, Australia.</p>
                  <p>
                    <span className="ftp-info">
                      <i className="fa fa-phone" aria-hidden="true" />
                      +1 246-345-0695
                    </span>
                  </p>
                  <p>
                    <span className="ftp-info">
                      <i className="fa fa-envelope" aria-hidden="true" />
                      info@example.com
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-2 col-md-4">
              <div className="footer-widget">
                <h4 className="widget-title">Navigations</h4>
                <ul className="footer-menu">
                  <li>
                    <a href="about-us.html">About Us</a>
                  </li>
                  <li>
                    <a href="faq.html">FAQs Page</a>
                  </li>
                  <li>
                    <a href="checkout.html">Checkout</a>
                  </li>
                  <li>
                    <a href="contact.html">Contact</a>
                  </li>
                  <li>
                    <a href="blog.html">Blog</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-2 col-md-4">
              <div className="footer-widget">
                <h4 className="widget-title">The Highlights</h4>
                <ul className="footer-menu">
                  <li>
                    <a>Apartment</a>
                  </li>
                  <li>
                    <a>My Houses</a>
                  </li>
                  <li>
                    <a>Restaurant</a>
                  </li>
                  <li>
                    <a>Nightlife</a>
                  </li>
                  <li>
                    <a>Villas</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-2 col-md-6">
              <div className="footer-widget">
                <h4 className="widget-title">My Account</h4>
                <ul className="footer-menu">
                  <li>
                    <a>My Profile</a>
                  </li>
                  <li>
                    <a>My account</a>
                  </li>
                  <li>
                    <a>My Property</a>
                  </li>
                  <li>
                    <a>Favorites</a>
                  </li>
                  <li>
                    <a>Cart</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-3 col-md-6">
              <div className="footer-widget">
                <h4 className="widget-title">Download Apps</h4>
                <div className="app-wrap">
                  <p>
                    <a>
                      <img
                        src="/src/assets/media/light-play.png"
                        className="img-fluid"
                        alt=""
                      />
                    </a>
                  </p>
                  <p>
                    <a>
                      <img
                        src="/src/assets/media/light-ios.png"
                        className="img-fluid"
                        alt=""
                      />
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-12 col-md-12">
              <p className="mb-0">© 2023 All Rights Reserved</p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
