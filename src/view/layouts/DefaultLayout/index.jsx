import Content from "./components/Content";
import Footer from "./components/Footer";
import Header from "pages/Header";

const Index = () => {
  return (
    <>
      <Header />
      <Content />
      <Footer />
    </>
  );
};

export default Index;
