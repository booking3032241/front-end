import { ToastContainer } from "react-toastify";
import DefautLayout from "./view/layouts/DefaultLayout";
import { AuthProvider } from "components/AuthContext";
function App() {
  return (
    <>
      <AuthProvider>
        <DefautLayout />
      </AuthProvider>
      <ToastContainer
        autoClose={2500}
        position="top-center"
        hideProgressBar={true}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </>
  );
}

export default App;
