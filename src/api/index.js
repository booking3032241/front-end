import axios from "axios";
import { getToken } from "helpers/cookies";

const baseURL = import.meta.env.VITE_API_URL;
let api = axios.create({
  baseURL: baseURL,
});

api.interceptors.request.use(
  function (config) {
    const token = getToken();
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    } else {
      config.headers["Authorization"] = "";
    }

    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

api.interceptors.response.use(
  async function (response) {
    return response;
  },
  function (error) {
    if (error.toJSON().message === "Network Error") {
      window.location.replace("/network-error");
    }

    return Promise.reject(error);
  }
);

export default api;
