import { BounceLoader } from "react-spinners";

const Loading = ({ isLoading, children }) => {
  if (!isLoading) return children;

  return (
    <div
      style={{
        display: "block",
        zIndex: 999,
        width: "100%",
        // minHeight: "100px" ,
        overflow: "auto",
        backgroundColor: "rgba(255, 255, 255, 0.4)" /* Black w/ opacity */,
      }}
    >
      <div className="d-flex justify-content-center align-items-center h-100">
        <BounceLoader color={"#36d7b7"} loading={isLoading} size={50} />
      </div>
    </div>
  );
};

export default Loading;
