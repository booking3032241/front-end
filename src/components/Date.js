import React from "react";
import Moment from "react-moment";

const Date = ({ date, withTime = true }) => {
  function renderTime() {
    if (date) {
      return (
        <Moment
          format={"DD/MM/YYYY" + (withTime ? "  hh:mm" : "")}
          date={date}
        ></Moment>
      );
    } else {
      return "";
    }
  }
  return <>{renderTime()}</>;
};

export default Date;
