import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";
import Empty from "components/Empty";

const MapPosition = ({ lat, lng }) => {
  const newLat = lat != null ? parseFloat(lat) : null;
  const newLng = lng != null ? parseFloat(lng) : null;
  console.log(newLat);
  console.log(newLng);
  return (
    <div className="d-flex justify-content-center">
      {newLat == null || newLng == null ? (
        <Empty
          description={"No location saved"}
          img_heigth="120px"
          img_width="120px"
        />
      ) : (
        <LoadScript
          googleMapsApiKey={import.meta.env.VITE_PUBLIC_GOOGLE_MAPS_API_KEY}
        >
          <GoogleMap
            mapContainerStyle={{
              height: "400px",
              width: "100%",
            }}
            zoom={12}
            center={{
              lat: newLat,
              lng: newLng,
            }}
          >
            <Marker
              position={{
                lat: newLat,
                lng: newLng,
              }}
            />
          </GoogleMap>
        </LoadScript>
      )}
    </div>
  );
};

export default MapPosition;
