import Slider from "react-slick";

const CustomSlider = ({ data, children }) => {
  var slidesToShow = data?.length > 1 ? 2 : 1;
  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: slidesToShow,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
    ],
  };
  return <Slider {...settings}>{children}</Slider>;
};

export default CustomSlider;
