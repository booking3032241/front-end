import { useEffect, useState } from "react";
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";
import usePlacesAutocomplete, {
  getGeocode,
  getLatLng,
} from "use-places-autocomplete";
import {
  Combobox,
  ComboboxInput,
  ComboboxPopover,
  ComboboxList,
  ComboboxOption,
} from "@reach/combobox";
import "@reach/combobox/styles.css";
import { setKey, fromLatLng } from "react-geocode";
import { Children } from "react";
import { cloneElement } from "react";

const getCountry = (addressArray) => {
  let country = "";
  for (let i = 0; i < addressArray.length; i++) {
    if (addressArray[i].types[0] && "country" === addressArray[i].types[0]) {
      country = addressArray[i].long_name;
      return country;
    }
  }
};

const getArea = (addressArray) => {
  let city = "";
  for (let i = 0; i < addressArray.length; i++) {
    if (
      addressArray[i].types[0] &&
      "administrative_area_level_2" === addressArray[i].types[0]
    ) {
      city = addressArray[i].long_name;
      return city;
    }
  }
};

const getCity = (addressArray) => {
  let area = "";
  for (let i = 0; i < addressArray.length; i++) {
    if (addressArray[i].types[0]) {
      for (let j = 0; j < addressArray[i].types.length; j++) {
        if (
          "sublocality_level_1" === addressArray[i].types[j] ||
          "locality" === addressArray[i].types[j] ||
          "establishment" === addressArray[i].types[j] ||
          "natural_feature" === addressArray[i].types[j]
        ) {
          area = addressArray[i].long_name;
          return area;
        }
      }
    }
  }
};

const getState = (addressArray) => {
  let state = "";
  for (let i = 0; i < addressArray.length; i++) {
    for (let i = 0; i < addressArray.length; i++) {
      if (
        addressArray[i].types[0] &&
        "administrative_area_level_1" === addressArray[i].types[0]
      ) {
        state = addressArray[i].long_name;
        return state;
      }
    }
  }
};

export default function Map({ children, lat, lng }) {
  const [address, setAddress] = useState("");
  const [country, setCountry] = useState("");
  const [city, setCity] = useState("");
  const [area, setArea] = useState("");
  const [state, setState] = useState("");
  const [selected, setSelected] = useState({
    lat: lat ? lat : 33.892166,
    lng: lng ? lng : 9.561555,
  });
  const mapStyles = {
    height: "400px",
    width: "100%",
  };
  const handleMarkerClick = (e) => {
    const lat = e.latLng.lat();
    const lng = e.latLng.lng();
    setSelected({
      lat: lat,
      lng: lng,
    });
    setKey(import.meta.env.VITE_PUBLIC_GOOGLE_MAPS_API_KEY);
    fromLatLng(lat, lng)
      .then(({ results }) => {
        const formatted_address = results[0].formatted_address,
          addressArray = results[0].address_components,
          country = getCountry(addressArray),
          city = getCity(addressArray),
          area = getArea(addressArray),
          state = getState(addressArray);

        setAddress(formatted_address ? formatted_address : "");
        setCountry(country ? country : "");
        setCity(city ? city : "");
        setArea(area ? area : "");
        setState(state ? state : "");
      })
      .catch(console.error);
  };
  let elements = Children.toArray(children);
  elements = elements.map((item) =>
    cloneElement(item, {
      address: address,
      country: country,
      city: city,
      area: area,
      state: state,
      latitude: selected.lat,
      longitude: selected.lng,
    })
  );
  useEffect(() => {
    if (lat && lng) {
      setSelected({
        lat: parseFloat(lat),
        lng: parseFloat(lng),
      });
    }
  }, [lat, lng]);

  return (
    <>
      <LoadScript
        googleMapsApiKey={import.meta.env.VITE_PUBLIC_GOOGLE_MAPS_API_KEY}
        libraries={["places"]}
      >
        <GoogleMap
          mapContainerStyle={mapStyles}
          zoom={12}
          center={selected}
          onClick={handleMarkerClick}
        >
          <div className="places-container">
            <PlacesAutocomplete
              setSelected={setSelected}
              setAddress={setAddress}
              setCountry={setCountry}
              setCity={setCity}
              setArea={setArea}
              setState={setState}
            />
          </div>
          {/* Add the Marker component */}
          <Marker
            position={selected}
            name={"Dolores park"}
            draggable={true}
            onDragEnd={handleMarkerClick}
          />
        </GoogleMap>
      </LoadScript>
      <div>{elements}</div>
    </>
  );
}

const PlacesAutocomplete = ({
  setSelected,
  setAddress,
  setCountry,
  setCity,
  setArea,
  setState,
}) => {
  const {
    ready,
    value,
    setValue,
    suggestions: { status, data },
    clearSuggestions,
  } = usePlacesAutocomplete();

  const handleSelect = async (address) => {
    setValue(address, false);
    clearSuggestions();

    const results = await getGeocode({ address });
    const formatted_address = results[0].formatted_address,
      addressArray = results[0].address_components,
      country = getCountry(addressArray),
      city = getCity(addressArray),
      area = getArea(addressArray),
      state = getState(addressArray);

    setAddress(formatted_address ? formatted_address : "");
    setCountry(country ? country : "");
    setCity(city ? city : "");
    setArea(area ? area : "");
    setState(state ? state : "");

    const { lat, lng } = await getLatLng(results[0]);
    setSelected({ lat, lng });
  };

  return (
    <Combobox onSelect={handleSelect}>
      <ComboboxInput
        value={value}
        onChange={(e) => setValue(e.target.value)}
        disabled={!ready}
        className="combobox-input"
        placeholder="Search an address"
      />
      <ComboboxPopover>
        <ComboboxList>
          {status === "OK" &&
            data.map(({ place_id, description }) => (
              <ComboboxOption key={place_id} value={description} />
            ))}
        </ComboboxList>
      </ComboboxPopover>
    </Combobox>
  );
};
