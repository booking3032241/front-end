import api from "api";
import { alertError } from "helpers/GeneralFunctions";
import { alertSuccess } from "helpers/GeneralFunctions";
import { removeUserInfos } from "helpers/cookies";
import { getUserInfos } from "helpers/cookies";
import { checkLoggedIn } from "helpers/cookies";
import { setUserInfos } from "helpers/cookies";
import { createContext, useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import ProfileImage from "assets/media/profile-image.jpg";
const AuthContext = createContext(null);

export const AuthProvider = ({ children }) => {
  const navigate = useNavigate();
  const loggedIn = checkLoggedIn();
  const user = getUserInfos();
  const userId = user ? user.id : "";
  const [customLoading, setCustomLoading] = useState(false);
  const [loginModalShow, setLoginModalShow] = useState(false);
  const [registerModalShow, setRegisterModalShow] = useState(false);
  const [userDefaultImage, setUserDefaultImage] = useState(ProfileImage);

  const login = (fields) => {
    setCustomLoading(true);
    api
      .post("auth/login", fields)
      .then((response) => {
        setUserInfos(response.data);
        setUserDefaultImage(response.data?.user?.defaultImageSrc);
        alertSuccess("Vous êtes authentifié.");
        setCustomLoading(false);
        showLoginModal(false);
        navigate("/", { replace: true });
      })
      .catch((error) => {
        var message = error.response.data;
        alertError(message, "error");
        setCustomLoading(false);
      });
  };
  const register = (fields) => {
    setCustomLoading(true);
    api
      .post("auth/register", fields)
      .then((response) => {
        setUserInfos(response.data);
        setUserDefaultImage(response.data?.user.defaultImageSrc);
        alertSuccess("Votre compte a été crée.");
        setCustomLoading(false);
        showRegisterModal(false);
        navigate("/", { replace: true });
      })
      .catch((error) => {
        console.log(error);
        var message = error.response.data;
        alertError(message, "error");
        setCustomLoading(false);
      });
  };

  const fetchDefaultUserImage = () => {
    if (userId) {
      setCustomLoading(true);
      api
        .get(`galleries/default?entityId=1&entityIdentifier=${userId}`)
        .then((response) => {
          setUserDefaultImage(response.data?.imageSrc);
          setCustomLoading(false);
        })
        .catch((error) => {
          var message = error.response.data;
          console.log(message);
          setCustomLoading(false);
        });
    }
  };

  const logout = () => {
    removeUserInfos();
    navigate("/", { replace: true });
  };
  const showLoginModal = (item) => {
    setLoginModalShow(item);
  };
  const showRegisterModal = (item) => {
    setRegisterModalShow(item);
  };
  useEffect(() => {
    fetchDefaultUserImage();
  }, []);

  return (
    <AuthContext.Provider
      value={{
        loggedIn,
        customLoading,
        loginModalShow,
        showLoginModal,
        registerModalShow,
        showRegisterModal,
        userDefaultImage,
        fetchDefaultUserImage,
        user,
        login,
        logout,
        register,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  return useContext(AuthContext);
};
