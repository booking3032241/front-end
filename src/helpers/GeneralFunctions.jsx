import { toast } from "react-toastify";

const options = {
  position: "top-center",
  autoClose: 2000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
};

export const alertError = (text) => {
  return toast.error(text, options);
};
export const alertSuccess = (text) => {
  return toast.success(text, options);
};
export const alertWarning = (text) => {
  return toast.warning(text, options);
};

export const renderNature = (value) => {
  if (value == 1) {
    return <div className="badge badge-warning">For Rent</div>;
  }
  if (value == 2) {
    return <div className="badge badge-danger">For Sale</div>;
  }
  return null;
};
export const addScript = (path) => {
  var elem = document.getElementById(path);
  if (elem) {
    elem.remove();
  }

  const script = document.createElement("script");

  script.src = path;
  script.id = path;
  script.class = "custom-script";
  script.async = true;

  document.body.appendChild(script);
}
