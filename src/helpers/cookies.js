import Cookies from "js-cookie";

export const setUserInfos = (result) => {
  removeUserInfos();
  Cookies.set("loggedIn", 1);
  Cookies.set("user", JSON.stringify(result.user));
  Cookies.set("token", result.token);
};

export const removeUserInfos = () => {
  Cookies.remove("user");
  Cookies.remove("loggedIn");
  Cookies.remove("token");
};

export const getUserInfos = () => {
  if (Cookies.get("user")) {
    return JSON.parse(Cookies.get("user"));
  }
  return null;
};
export const getToken = () => {
  if (Cookies.get("token")) {
    return Cookies.get("token");
  }
  return null;
};
export const checkLoggedIn = () => {
  if (Cookies.get("loggedIn")) {
    return Cookies.get("loggedIn");
  }
  return null;
};
